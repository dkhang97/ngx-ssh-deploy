# ngx-ssh-deploy

[![version](https://img.shields.io/npm/v/ngx-ssh-deploy.svg?style=flat)](https://www.npmjs.com/package/ngx-ssh-deploy) [![npm](https://img.shields.io/npm/l/ngx-ssh-deploy.svg)](https://opensource.org/licenses/MIT)

## Installation

```bash
npm i -D ngx-ssh-deploy
```

or globally install the package

```bash
npm i -g ngx-ssh-deploy
```

Or using `ng add`

```bash
ng add ngx-ssh-deploy
```

## Usage

### Initialize configuration

```bash
ngx-ssh-deploy init
```

Or init with host, user and pem file and deployment path

```bash
ngx-ssh-deploy init ec2-user@ec2-127-0-0-1.example.compute.amazonaws.com/sub -K ./certs/example.com/example.pem -P /home/ec2-user/angular-app
```

Or using `ng add`

```bash
ng add ngx-ssh-deploy --endpoint ec2-user@ec2-127-0-0-1.example.compute.amazonaws.com/sub --pem-file ./certs/example.com/example.pem --ssh-path /home/ec2-user/angular-app --project my-angular-app
```

### Deploy the app

```bash
ngx-ssh-deploy deploy
```

### Generate web server configuration

for Apache

```bash
ngx-ssh-deploy config apache
```

or nginx

```bash
ngx-ssh-deploy config nginx
```

## CLI

```text
ngx-ssh-deploy [command]

Commands:
  init [endpoint]                               Init ngx-ssh-deploy
  config <serverName> [environment]             Generate server configuration
  deploy [environment]                          Deploy Angular to server using SSH
  build [environment]                           Build project and run post build command(s) if any. Useful to run in pipeline, e.g., docker
  stats [environment]                           Run webpack-bundle-analyzer for specific deploy environment
  update-target-login [environment]             Generate shell commands to update target SSH login authorized_keys
Options:
  --help     Show help                                                 [boolean]
  --version  Show version number                                       [boolean]
```

### Example Configuration (ngx-ssh-deploy.json)

```json
{
  "$schema": "./node_modules/ngx-ssh-deploy/ngx-ssh-deploy.schema.json",
  "environments": {
    "angular-app": {
      "directDeploy": false,
      "endpoint": {
        "host": "ec2-127-0-0-1.example.compute.amazonaws.com",
        "port": 22,
        "user": "ec2-user",
        "pemFile": "./certs/example.com/example.pem"
      },
      "ssh": {
        "path": "/home/ec2-user/angular-app"
      },
      "nodeOptions": {
        "max_old_space_size": 8096
      },
      "postBuildCommand": "npx cpx {project.sourceRoot}{sep}pwa{sep}manifest.webmanifest {dirDist}{sep}",
      "projectConfiguration": "production",
      "targetUrl": "https://example.com/"
    }
  },
  "defaultEnvironment": "angular-app"
}
```

or **angular.json** projects/\<project>/architect `ng deploy`

```json
{
    "deploy": {
        "builder": "ngx-ssh-deploy:deploy",
        "options": {
            "directDeploy": false,
            "endpoint": {
                "host": "ec2-127-0-0-1.example.compute.amazonaws.com",
                "port": 22,
                "user": "ec2-user",
                "pemFile": "./certs/example.com/example.pem"
            },
            "ssh": {
                "path": "/home/ec2-user/angular-app"
            },
            "postBuildCommand": "npx cpx {project.sourceRoot}{sep}pwa{sep}manifest.webmanifest {dirDist}{sep}",
            "projectConfiguration": "production",
            "targetUrl": "https://example.com/"
        }
    }
}
```
