import { strip, yellowBright } from 'cli-color';
import prettyMs from 'pretty-ms';
import { combineLatest, defer, from, interval, Observable, of, Subject } from 'rxjs';
import { catchError, delayWhen, map, scan, shareReplay, startWith, switchMap, take, timeout } from 'rxjs/operators';

import { Twister } from './spinner';
import { exceptionWriter } from './utils';

const DEFAULT_PROCESS_MSG = 'Processing...';
const MSG_TIMEOUT: unknown = new Object();

export class ProgressLogger {
    constructor(
        private delayUntilObs: Observable<any> = of(undefined),
    ) { }

    log<T>(message: string | Observable<string>, obsOrTask: Observable<T> | (() => Promise<T>)) {
        const msgObs = message instanceof Observable ? message.pipe(
            timeout(500),
            catchError(() => of(MSG_TIMEOUT)),
            scan((prv, curr: string) => curr === MSG_TIMEOUT ? prv : curr, DEFAULT_PROCESS_MSG),
            shareReplay(1),
        ) : of(message);

        return new Observable<T>(subscriber => {
            const completeSbj = new Subject();
            const newObs = (obsOrTask instanceof Observable ? obsOrTask : from(obsOrTask()));

            const startTime = new Date().getTime();
            const endTimePromise = completeSbj.pipe(take(1),
                map(() => new Date().getTime())).toPromise();

            const durationTask = endTimePromise.then(endTime => endTime - startTime);

            const startElapsedTask = Promise.race([
                durationTask,
                new Promise<number>(resolve => setTimeout(() => resolve(undefined), 10)),
            ]);

            const mapMsg = (elapsedTask: number | Promise<number> = startElapsedTask) => {
                return from(Promise.resolve(elapsedTask)).pipe(
                    switchMap(elapsed => msgObs.pipe(map(msg => isNaN(elapsed) ? msg
                        : (msg + ' ' + yellowBright('(' + prettyMs(elapsed) + ')') + '  '))))
                );
            };

            const twTask = defer(async () => {
                const twMsgObs = mapMsg();
                const msg = await twMsgObs.pipe(take(1)).toPromise();
                const tw = new Twister(msg);
                subscriptions.push(twMsgObs.pipe(
                    delayWhen(() => this.delayUntilObs),
                ).subscribe(m => tw.text = m));
                return tw;
            }).pipe(shareReplay(1));
            const subscriptions = [
                newObs.subscribe(val => {
                    subscriber.next(val);
                }, async err => {
                    await Promise.all([startElapsedTask,
                        twTask.pipe(switchMap(async tw => {
                            tw.fail(await mapMsg().toPromise());
                            await tw.obs.pipe(take(1)).toPromise();
                        })).toPromise(),
                        (await exceptionWriter.toPromise()).write(err,
                            strip(await msgObs.pipe(take(1)).toPromise())),
                    ]);
                    subscriber.error(err);
                }, async () => {
                    await Promise.all([
                        twTask.pipe(switchMap(tw => tw.obs.pipe(take(1)))).toPromise(),
                    ]);

                    completeSbj.next();
                    subscriber.complete();
                }),
                completeSbj.pipe(
                    delayWhen(() => this.delayUntilObs),
                    take(1),
                ).subscribe(() => {
                    twTask.subscribe(tw => tw.succeed());
                }),
                combineLatest([
                    interval(process.stdout.isTTY ? 100 : 20_000).pipe(
                        map(async () => new Date().getTime() - startTime),
                        startWith(startElapsedTask),
                        switchMap(s => s),
                    ),
                    from(durationTask).pipe(
                        startWith(Number.MAX_VALUE),
                    ),
                ]).pipe(
                    delayWhen(() => this.delayUntilObs),
                    map(ts => Math.min(...ts)),
                    switchMap(elapsed => combineLatest([twTask, mapMsg(elapsed)])),
                ).subscribe(([spinner, msg]) => spinner.text = msg),
            ];

            return {
                unsubscribe() {
                    twTask.subscribe(tw => tw.destroy());
                    subscriptions.forEach(s => s.unsubscribe());
                }
            }
        });
    }
}

export async function disposableLogger<T>(logMsg: Observable<string>, task: () => T | Promise<T>): Promise<T> {
    const msg = await logMsg.pipe(take(1)).toPromise();
    const tw = new Twister(msg);
    logMsg.subscribe(msg => tw.text = msg);

    try {
        return await Promise.resolve(task());
    } finally {
        tw.remove();
    }
}
