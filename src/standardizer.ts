import { sortedUniqBy } from 'lodash-uni';
import { render as mRender } from 'mustache';

import { DeployEnvironment, DeployProjectConfiguration } from './schema';

export interface StandardizedDeployEnvironment extends Omit<DeployEnvironment, 'projectConfiguration' | 'ssh' | 'postBuildCommand' | 'postDeployCommand'> {
    name: string,
    projectConfiguration: DeployProjectConfiguration[],
    ssh: Omit<DeployEnvironment['ssh'], 'path'> & { path: Record<string, string> },
    postBuildCommand: Record<string, string[]>;
    postDeployCommand: string[];
};

export function resolveBuildConfiguration(configuration: DeployEnvironment['projectConfiguration']): DeployProjectConfiguration[] {
    const allConfigs = (Array.isArray(configuration) ? configuration : [configuration])
        .map(cfg => typeof cfg === 'object' ? cfg : { configuration: cfg });

    if (!allConfigs.length) {
        allConfigs.push(null);
    }

    const filledConfigs = allConfigs.map(cfg => ({
        architect: cfg?.architect ?? 'build',
        configuration: cfg?.configuration ?? 'production',
    }));

    return sortedUniqBy(filledConfigs, cfg => JSON.stringify(cfg));
}

function resolveStringArray(value: string | string[]): string[] {
    return Array.isArray(value) ? value : value ? [value] : [];
}

export function standardizeDeployEnvironment(env: DeployEnvironment, name: string) {
    const projectConfiguration = resolveBuildConfiguration(env.projectConfiguration);

    return {
        ...env,
        name,
        projectConfiguration,
        ssh: {
            ...env.ssh,
            path: typeof env.ssh.path !== 'string' ? env.ssh.path
                : { [projectConfiguration[0]['architect']]: env.ssh.path },
        },
        postBuildCommand: (cmd => {
            if (cmd === null || cmd === undefined) {
                return {};
            }

            if (typeof cmd !== 'object') {
                const build = resolveStringArray(cmd);
                return build.length ? { build } : {};
            }

            return Object.assign({}, ...Object.entries(cmd).map(([key, value]) => {
                const val = resolveStringArray(value);

                if (val.length) {
                    return { [key]: val };
                }
            }));
        })(env.postBuildCommand),
        postDeployCommand: resolveStringArray(env.postDeployCommand),
    } as StandardizedDeployEnvironment;
}

export function mustacheRender(template: string, object: Object) {
    return mRender(template, object, undefined, {
        escape: val => typeof val === 'object'
            ? JSON.stringify(val) : String(val),
        tags: ['{', '}'],
    }).trim()
}
