#!/usr/bin/env node
import { reset, yellowBright } from 'cli-color';
import { command } from 'yargs';

import serverConfigs from './server-configs';
import { TWISTERS } from './spinner';
import { exitSignal, packageJson } from './utils';

exitSignal(() => {
    TWISTERS.log('');
    process.stderr.write(['\u001B[?25h', '', '', reset, yellowBright('Terminated!'), ''].join(require('os').EOL));
    process.exit(1);
}, 999);

function requireCommand(path: string) {
    const endFn = () => {
        process.stderr.write('\u001B[?25h\n\n');
        process.exit();
    }

    return async function (...args: any[]) {
        const fn = require(path);

        if (typeof fn === 'function') {
            console.log();
            const task = fn(...args);

            if (task instanceof Promise) {
                const preventExitInterval = setInterval(() => { /* prevent program exit */ }, 10);
                try {
                    await task;
                } finally {
                    clearInterval(preventExitInterval);
                    endFn();
                }
            } else {
                endFn();
            }
        }
    };
}

command('init [endpoint]', `Init ${packageJson.name}`,
    yargs => yargs.positional('endpoint', {
        desc: 'Endpoint of SSH server',
    }).option('pem-file', {
        alias: 'K',
        desc: 'RSA key file for authentication'
    }).option('ssh-path', {
        alias: 'P',
        desc: 'SSH target path',
    }),
    requireCommand('./commands/init')
).command('config <serverName> [environment]', 'Generate server configuration',
    yargs => yargs.positional('serverName', {
        desc: 'Server Name',
        choices: Object.keys(serverConfigs),
    }).positional('environment', {
        desc: 'Deploy environment',
    }).option('output-file', {
        alias: 'O',
        desc: 'Output file for configuration',
    }),
    requireCommand('./commands/config')
).command('deploy [environment]', 'Deploy Angular to server using SSH',
    yargs => yargs.positional('environment', {
        desc: 'Deploy environment',
    }).option('build', {
        alias: 'B',
        desc: 'Build Angular project before deploy',
        boolean: true,
    }).option('build-verbose', {
        alias: 'D',
        desc: 'Show detached console for Angular build',
        boolean: true,
    }).option('pwd', {
        alias: 'P',
        desc: 'Password of user or passphrase for private key',
        boolean: true,
    }).option('build-concurrency', {
        alias: 'T',
        desc: 'Number of concurrent ng build',
        type: 'number',
    }).check(({ build, buildVerbose, buildConcurrency }) => {
        if (typeof buildConcurrency === 'number' && buildConcurrency <= 0) {
            throw new Error('Build concurrency must be greater than 0');
        }

        if (buildVerbose && !build) {
            throw new Error('`build-verbose` option must come along with `build` option');
        }

        return true;
    }),
    requireCommand('./commands/deploy')
).command('build [environment]', 'Build project and run post build command(s) if any. Useful to run in pipeline, e.g., docker',
    yargs => yargs.positional('environment', {
        desc: 'Deploy environment',
    }).option('output-path', {
        alias: 'O',
        desc: 'Output path for build',
    }).option('architect', {
        alias: 'A',
        desc: 'Specify `architect` in angular.json project',
    }),
    requireCommand('./commands/build')
).command('stats [environment]', 'Run webpack-bundle-analyzer for specific deploy environment',
    yargs => yargs.positional('environment', {
        desc: 'Deploy environment',
    }).option('forceBuild', {
        alias: 'B',
        desc: 'Force project build despite stats.json exists',
        boolean: true,
    }).option('architect', {
        alias: 'A',
        desc: 'Specify `architect` in angular.json project',
    }),
    requireCommand('./commands/stats')
).command('update-target-login [environment]', 'Generate shell commands to update target SSH login authorized_keys',
    yargs => yargs.positional('environment', {
        desc: 'Deploy environment',
    }).option('file', {
        alias: 'f',
        desc: 'Output shell file',
    }),
    requireCommand('./commands/update-target-login')
).argv;