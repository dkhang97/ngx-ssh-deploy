import { red } from 'cli-color';
import onDeath from 'death';
import { appendFile, readFile } from 'fs';
import fsExtra from 'fs-extra';
import { sortBy } from 'lodash-uni';
import { EOL, tmpdir } from 'os';
import fsPath from 'path';
import prompts from 'prompts';
import { Observable, ObservableInput, ObservedValueOf, OperatorFunction, defer, pipe } from 'rxjs';
import { delayWhen, map, shareReplay, withLatestFrom } from 'rxjs/operators';
import sanitize from 'sanitize-filename';
import temp from 'temp';

import { ensureAbsolutePath } from './file-utils';
import { DeployProjectConfiguration } from './schema';
import { resolveBuildConfiguration } from './standardizer';

export const packageJson: { name: string, version: string } = require('../package.json');

export const stdinObs = new Observable<string | Buffer>(subscriber => {
    const readableFn = () => {
        let chunk: Buffer | string;
        while ((chunk = process.stdin.read()) !== null) {
            subscriber.next(chunk);
        }
    };

    process.stdin.on('readable', readableFn);
    return { unsubscribe() { process.stdin.off('readable', readableFn); } };
});

const exitActions: { priority?: number, task(): void }[] = [];

onDeath({ SIGINT: true })(() => {
    sortBy(exitActions, 'priority').forEach(a => {
        a.task();
    });
});

export function toPromise<T>(callbackFn: (callback: (err: any, result: T) => any) => void, ignoreError?: boolean) {
    return new Promise<T>((resolve, reject) =>
        callbackFn((err, result) => {
            if (err) {
                if (ignoreError) {
                    resolve(undefined);
                } else {
                    reject(err);
                }
            } else {
                resolve(result);
            }
        }));
}

export async function readLocal<T = string>(fileName: string, convertFn?: (data: Buffer) => T): Promise<T> {
    try {
        const data = await toPromise<Buffer>(callback => readFile(fileName, callback));
        return typeof convertFn === 'function' ? convertFn(data) : <any>data.toString();
    } catch (e) {
        (await exceptionWriter.toPromise()).write(e, 'Read file error: ' + fileName);
        return null;
    }
}

export async function prompt2<T = string>(message: string, opts?: Partial<prompts.PromptObject>) {
    return (await prompts({ type: 'text', ...opts, message, name: 'value' })).value as T;
}

export function exitSignal(task: () => void, priority = 0) {
    if (typeof task === 'function') {
        exitActions.unshift({ task, priority });
    }
}

let appErrorFileName: string;

export const exceptionWriter = defer(async () => {
    const dir = fsPath.join(await tempAppDir.toPromise(), 'errors');

    await fsExtra.ensureDir(dir);

    appErrorFileName = temp.path({
        dir, suffix: '.log',
    });

    return {
        write(err: Error, message?: string) {
            const txt = [
                '===',
                `${new Date().toLocaleString()} ${message || 'Error'}`,
                err.stack.replace(/\r?\n/g, EOL),
            ];

            return new Promise((fResolve, fReject) => appendFile(appErrorFileName, txt.join(EOL), err => {
                if (err) {
                    fReject(err);
                } else {
                    fResolve(undefined);
                }
            }));
        }
    };
}).pipe(shareReplay(1));

export function logAndThrow(err: Error, message?: string): never {
    exceptionWriter.subscribe(e => e.write(err, message));
    throw err;
}

export function withDeferred<T, D extends ObservableInput<any> | void>(deferred: () => D): OperatorFunction<T, [T, ObservedValueOf<D>]> {
    const df = defer(deferred).pipe(shareReplay(1));

    return pipe(
        delayWhen(() => df),
        withLatestFrom(df),
    );
}

process.on('exit', () => {
    if (appErrorFileName) {
        console.log(red(`Error(s) are written to ${appErrorFileName}`))
    }
});

export function forceStringArray(input: string | string[]): string[] {
    return [].concat(input).filter(v => typeof v === 'string');
}

export const tempAppDir = defer(async () => {
    let tmpPath = ensureAbsolutePath(process.env.DEPLOY_CACHE_DIR || tmpdir());

    const path = fsPath.join(tmpPath, '.' + sanitize(packageJson.name));
    await fsExtra.ensureDir(path);
    return path;
}).pipe(shareReplay(1));

export interface ProjectBuildCache {
    source: {
        path: string;
        project: string;
        configuration: DeployProjectConfiguration[];
    }

    distPath: Record<string, string>;
    buildChecksum?: string;
    sourceChecksum?: string;
    sourceStatChecksum?: string;
    nodeModulesChecksum?: string;
    packageJsonChecksum?: string;
    libChecksum?: Record<string, string>;
    [key: `buildChecksum_${string}`]: string;
}

export const tmpDb = new (class {
    readonly filePath = defer(() => tempAppDir.pipe(
        map(dir => fsPath.join(dir, 'db.json')),
        shareReplay(1),
    ));

    data: {
        buildCache?: ProjectBuildCache[],
        postBuildDist?: Record<string, { path?: string, checksum?: string, commands?: string[] }>,
    };

    async read() {
        await fsExtra.readJSON(await this.filePath.toPromise()).catch(() => ({}))
            .then(({ buildCache, ...json } = {}) => this.data = {
                ...json,
                buildCache: buildCache?.map(({ source: { configuration, ...src }, distPath, ...cache }) => ({
                    ...cache,
                    distPath: typeof distPath === 'string' ? { build: distPath } : distPath,
                    source: {
                        ...src, configuration: resolveBuildConfiguration(configuration),
                    },
                })) || [],
            });
    }

    async write() {
        await fsExtra.writeJSON(await this.filePath.toPromise(), this.data, { spaces: 2 }).catch(() => { });
    }
})();


// onDeath({ uncaughtException: true })(() => {
//     debugger;
// });