import { readLocal } from '../utils';
import fsPath from 'path';
import glob from 'glob';
import { iterateFiles } from '../file-utils';

export type NgProjectAssetEntry<T = {}> = string | ({ input: string } & T);

export interface NgProjectOptions {
    outputPath?: string;
    main?: string;
    tsConfig?: string;
    fileReplacements?: { replace?: string, with?: string, src?: string, replaceWith?: string }[];
    index?: string;
    polyfills?: string;
    assets?: NgProjectAssetEntry<{ glob: string }>[];
    scripts?: NgProjectAssetEntry[];
    styles?: NgProjectAssetEntry[];
}

export interface NgProject {
    sourceRoot: string;
    architect: Record<'build' | 'server', {
        options: NgProjectOptions,
        configurations: Record<string, NgProjectOptions>,
    }>
}

export interface AngularConfig {
    projects: Record<string, NgProject>;
    defaultProject: string;
}

const jsonTask = readLocal<AngularConfig>('angular.json', d => JSON.parse(d.toString()));


export default jsonTask;

export async function resolveAngularProjectOptions(architect: keyof NgProject['architect'], project?: string, configuration?: string) {
    const json = await jsonTask;

    if (!json) {
        return;
    }

    const prj = json.projects[project ?? json.defaultProject];
    const arc = prj?.architect?.[architect];

    if (!arc) { return; }

    return Object.assign({}, arc.options, arc.configurations?.[configuration]);
}

export function* iterateNgAssets(assets: NgProjectAssetEntry<{ glob?: string }>[], basePath = process.cwd()) {
    if (Array.isArray(assets)) {
        for (const entry of assets) {
            if (!entry) { continue; }

            if (typeof entry === 'string') {
                yield* iterateFiles(fsPath.join(basePath, entry));
            } else if (typeof entry === 'object' && entry.input) {
                if (entry.glob) {
                    const result = glob.sync(entry.glob, {
                        root: basePath,
                        cwd: entry.input,
                        absolute: true,
                    });

                    for (const globEntry of result) {
                        yield* iterateFiles(globEntry);
                    }
                } else {
                    yield* iterateFiles(fsPath.join(basePath, entry.input));
                }
            }
        }
    }
}