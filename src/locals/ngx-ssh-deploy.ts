import cliColor from 'cli-color';
import { isEqual, uniq } from 'lodash-uni';
import { nanoid } from 'nanoid';
import fsPath, { join as joinPath, resolve } from 'path';
import { forkJoin, from, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import sanitize from 'sanitize-filename';
import Url from 'url-parse';

import { DeployEnvironment } from '../schema';
import {
    StandardizedDeployEnvironment,
    mustacheRender,
    resolveBuildConfiguration,
    standardizeDeployEnvironment,
} from '../standardizer';
import { ProjectBuildCache, packageJson, prompt2, readLocal, tempAppDir, tmpDb } from '../utils';
import angularConfig, { NgProject } from './angular';

export interface DeployConfig {
    $schema?: string;
    environments: Record<string, DeployEnvironment>;
    defaultEnvironment?: string | string[];
}

export interface DeployOpts {
    name: string;
    sshEndPoint: string;
    env: StandardizedDeployEnvironment;
    baseHref: string;
    url: Url;
    path: string;
    buildPath: Record<string, string>;
    buildCache: ProjectBuildCache;
    buildParameters: Record<string, { project: string, configuration?: string }>;
    buildCommand: Record<string, string[]>;
    localBuildCommand: Record<string, string[]>;
}

const JSON_FILE = sanitize(packageJson.name) + '.json';

const jsonTask = readLocal<DeployConfig>(JSON_FILE, d => JSON.parse(d.toString()));


export async function getDeployEnvironment(environment: string) {
    const { environments } = (await jsonTask) || <DeployConfig>{};
    jsonTask.then(f => {
        if (!f) {
            console.log(cliColor.red(resolve(JSON_FILE) + ' not found!'));
        }
    });

    if (!environments || typeof environments !== 'object') {
        return null;
    }

    const env = environments[environment];

    if (env) {
        return standardizeDeployEnvironment(env, environment);
    } else {
        console.log(cliColor.red(`Parameter 'environment' is not specified.`));
        return null;
    }
}

function parseUrl(uriText: string) {
    let txt = typeof uriText === 'string' ? uriText.trimLeft() : '';

    if (!/^[-_\w]+:\/\//.exec(txt)) {
        txt = 'https://' + txt;
    }

    return new Url(txt);
}

export async function getDeployOpts(environment: string | StandardizedDeployEnvironment) {
    const env = typeof environment !== 'string' ? environment : await getDeployEnvironment(environment);

    if (!env) {
        return null;
    }

    const endpointHost = env.endpoint.host;
    const [sshEndPoint] = (endpointHost || '').split('/');

    const url = parseUrl(env.targetUrl || endpointHost);
    let path = url.pathname.trimLeft();

    if (path.endsWith('/')) {
        path = path.substring(0, path.length - 1);
    }

    const defaultProject = async () => (await angularConfig)?.defaultProject;

    const source = {
        path: resolve('.'),
        project: env.ngProject ?? await defaultProject(),
        configuration: resolveBuildConfiguration(env.projectConfiguration)
    };

    const gBuildCache = tmpDb.data?.buildCache || (tmpDb.data = Object.assign({}, tmpDb.data, { buildCache: [] } as unknown)).buildCache;

    const allDist: string[] = [].concat(...gBuildCache.map(g => Object.values(g?.distPath || {})));

    let buildCache = gBuildCache.find(c => isEqual(c?.source, source));
    let idGenerated: boolean;
    const genId = () => {
        idGenerated = true;
        return nanoid();
    }

    if (!buildCache) {
        idGenerated = true;
        gBuildCache.push(buildCache = { source, distPath: {} });
    }

    const architects = uniq(source.configuration.map(c => c.architect));

    for (const architect of architects) {
        if (!buildCache.distPath?.[architect]) {
            let distPath: string;
            while (!distPath || allDist.includes(distPath)) {
                distPath = genId();
            }

            buildCache.distPath = Object.assign({}, buildCache.distPath, { [architect]: distPath });
        }
    }

    if (idGenerated) {
        await tmpDb.write();
    }

    const buildPath: DeployOpts['buildPath'] = {};
    const localBuildCommand: DeployOpts['localBuildCommand'] = {};
    const buildCommand: DeployOpts['buildCommand'] = {};
    const buildParameters: DeployOpts['buildParameters'] = {};
    for (const { architect, configuration } of source.configuration) {
        buildPath[architect] = joinPath(await tempAppDir.toPromise(), 'builds', buildCache.distPath[architect]);

        const buildParam = buildParameters[architect] = {
            project: env.ngProject ?? await defaultProject(),
            configuration,
        };

        localBuildCommand[architect] = [
            'ng', 'run', `${buildParam.project}:${architect}`,
            ...(configuration ? ['--configuration', configuration] : []),
            ...(() => {
                switch (architect) {
                    case 'build': return ['--base-href', path + '/'];
                    case 'server': return ['--deploy-url', path + '/'];
                }
                return [];
            })(),
        ];
        buildCommand[architect] = [...localBuildCommand[architect], '--output-path', buildPath[architect]];
    }

    return {
        name: env.name, baseHref: path + '/',
        sshEndPoint, env, url, path, buildCache, buildPath,
        localBuildCommand, buildCommand, buildParameters,
    } as DeployOpts;
}

function cliTemplateColors(formatKey?: (key: string) => string) {
    try {
        const colors = Object.keys(require('cli-color/lib/sgr').mods);
        return Object.assign({}, ...colors.map(k => ({ [formatKey?.(k) ?? k]: () => (text, render) => cliColor[k](render(text)) })));
    } catch { return {}; }
}

export function renderEnvironmentTitle(opt: DeployOpts) {
    const env = opt?.env;

    if (!env) {
        return;
    }

    const { title, ...envVar } = env;

    if (typeof title === 'string') {
        return mustacheRender(title, Object.assign({},
            envVar, {
            url: opt.url.href,
            path: opt.path,
            baseHref: opt.baseHref,
            color: cliTemplateColors(),
        }));
    }

    return `${cliColor.blueBright(env.name)} (${opt.url.href})`;
}


export function promptSingleEnvironment(argEnv: string) {
    return from(jsonTask).pipe(
        switchMap(({ environments }) => {
            if (argEnv) {
                return from(getDeployOpts(argEnv))
            }

            const envNames = Object.keys(environments);

            switch (envNames.length) {
                case 0:
                    return throwError(new Error('No environment found!'));
                case 1:
                    return from(getDeployOpts(envNames[0]))
            }

            return forkJoin(envNames.map(n => getDeployOpts(n))).pipe(
                switchMap(envs => prompt2<DeployOpts>('Select deploy environment', {
                    type: 'select', choices: envs.map(e => ({
                        title: renderEnvironmentTitle(e),
                        value: e,
                    }))
                })),
            );
        })
    );
}


export function renderCommand(template: string, args: {
    project: NgProject,
    deployOptions: DeployOpts,
    dist?: string,
    extra?: any,
}) {
    const opt = args.deployOptions;

    return mustacheRender(template, Object.assign({
        project: args.project,
        environment: opt.env,
        baseHref: opt.baseHref,
        dirDeploy: opt.env.ssh.path,
        dirDist: args.dist ?? opt.buildPath,
        sep: fsPath.sep,
    }, args.extra));
}

export default jsonTask;