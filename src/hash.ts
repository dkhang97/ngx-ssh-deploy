import { createHash } from 'crypto';
import fsExtra from 'fs-extra';
import fsPath from 'path';

import { iterateFiles } from './file-utils';
import { workerWrap } from './worker';

export async function computeHash(basedir: string, files?: string[]) {
    const hash = createHash('sha1');

    for (let file of new Set([...(files ?? iterateFiles(basedir))])) {
        if (fsPath.isAbsolute(file)) {
            file = fsPath.relative(basedir, file);
        }

        const fullPath = fsPath.join(basedir, file);
        const statInfo = await fsExtra.stat(fullPath);
        const fileInfo = `${file}:${statInfo.size}:${statInfo.mtimeMs}`;
        hash.update(fileInfo);

        const len = 8;
        const buf = Buffer.alloc(len);
        const fd = await fsExtra.open(fullPath, 'r');

        try {
            await fsExtra.read(fd, buf, 0, len, 0);
        } finally {
            await fsExtra.close(fd);
        }
        hash.update(buf);
    }

    return hash.digest('base64').substring(0, 32);
}

export const worker = workerWrap(__filename, { computeHash });

export const computeHashWorker = worker.computeHash;


export async function pathChecksum(path: string) {
    if (!path) { return; }

    return computeHashWorker(path).catch(() => undefined as never);
    // return hashElement(path)
    //     .then(h => h?.hash).catch(() => undefined);
}
