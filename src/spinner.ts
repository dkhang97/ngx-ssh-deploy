import { cyan } from 'cli-color';
import { uniqueId } from 'lodash-uni';
import logSymbols from 'log-symbols';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { concatMap, debounceTime, filter, shareReplay } from 'rxjs/operators';
import { Twisters } from 'twisters';

import { exitSignal } from './utils';

export const TWISTERS = new Twisters({
    // spinner: terminalSupportsUnicode() ? {
    //     ...dots, frames: dots.frames.map(fr => cyan(fr)),
    // } : dashes,
    spinner: {
        interval: 50, frames: [
            // '.  ', '.. ', '...', ' ..', '  .', '   ',
            // '-  ', ' - ', '  -', '   ',
            // '*  ', '** ', '***', ' **', '  *', '   ',
            // '-  ', ' - ', '  -', '   ',
            '>  ', '>> ', '>>>', ' >>', '  >', '  +',
            '  <', ' <<', '<<<', '<< ', '<  ', '+  ',
        ].map(fr => cyan(fr)),
    },
});

const twSbj = new Subject<{ twId: string, val: { active: boolean, text: string, removed: boolean } }>();
const twSub = twSbj.subscribe(({ twId, val }) => TWISTERS.put(twId, val));
exitSignal(() => twSub.unsubscribe());

export class Twister {
    private textSbj = new Subject<string>();
    private prefixSbj = new BehaviorSubject<string>('');
    private removeSbj = new BehaviorSubject(false);

    readonly obs = combineLatest([this.textSbj.pipe(filter(t => !!t)), this.prefixSbj, this.removeSbj]).pipe(
        debounceTime(10), shareReplay(1),
    );

    set text(text: string) {
        this.textSbj.next(text);
    }

    constructor(
        text: string,
    ) {
        this.textSbj.next(text);

        const twId = uniqueId('twisters-');
        // TWISTERS.put(twId, { text });
        // const msgMap = TWISTERS['messages'];

        this.obs.subscribe(
            ([text, prefix, removed]) => {
                twSbj.next({
                    twId, val: {
                        active: !prefix, removed,
                        text: [prefix, text].filter(t => !!t).join(' '),
                    }
                });
            }
        );

        exitSignal(() => {
            this.destroy();
        });
    }

    succeed(text?: string) {
        // if (!this.prefixSbj.getValue()) {
        this.text = text;
        this.prefixSbj.next(logSymbols.success);
        // }
    }

    fail(text?: string) {
        // if (!this.prefixSbj.getValue()) {
        this.text = text;
        this.prefixSbj.next(logSymbols.error);
        // }
    }

    remove() {
        this.removeSbj.next(true);
    }

    destroy() {
        this.textSbj.complete();

        if (!this.prefixSbj.getValue()) {
            this.prefixSbj.next(logSymbols.warning);
        }
        this.prefixSbj.complete();
        // this.subscription.unsubscribe();
    }
}
