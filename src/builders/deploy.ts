import '../commands/deploy/sm/deploy-effects';

import { BuilderContext, BuilderOutput, createBuilder } from '@angular-devkit/architect';
import { of, timer } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { createSshPrompt } from '../commands/deploy/connect-ssh';
import { BuildProjectAction, FinishAction, SetLoggerAction, stama } from '../commands/deploy/sm/deploy-actions';
import { resolveFinishMessage } from '../commands/deploy/utils';
import { DeployOpts, getDeployOpts } from '../locals/ngx-ssh-deploy';
import { ProgressLogger } from '../log-progress';
import { NgxSshDeployAngularOptions } from '../schema';
import { TWISTERS } from '../spinner';
import { standardizeDeployEnvironment } from '../standardizer';
import { tmpDb } from '../utils';

// Call the createBuilder() function to create a builder. This mirrors
// createJobHandler() but add typings specific to Architect Builders.
export default createBuilder(
    async (options: NgxSshDeployAngularOptions, context: BuilderContext): Promise<BuilderOutput> => {
        if (!context.target) {
            throw new Error('Cannot deploy the application without a target');
        }

        const { build: ngBuildOpt, $schema, ...env } = options;

        await tmpDb.read();

        const deployOpt: DeployOpts = await getDeployOpts({
            ...standardizeDeployEnvironment(env, context.target.project),
            ngProject: context.target.project,
        });
        const sshFinishedObs = createSshPrompt(of(deployOpt));

        const logger = new ProgressLogger(sshFinishedObs);
        SetLoggerAction.dispatch({ logger });

        const projectMeta = await context.getProjectMetadata(context.target.project);

        sshFinishedObs.subscribe(() => {
            const { always, verbose }: { always?: boolean, verbose?: boolean } = (ngBuildOpt === true && {
                always: true
            }) || ngBuildOpt || {} as any;
            BuildProjectAction.dispatch({
                opt: deployOpt,
                project: projectMeta as any,
                build: always,
                buildVerbose: verbose,
            })
            stama.complete(BuildProjectAction);
        });

        return stama.onAction(FinishAction).pipe(
            take(1),
            switchMap(async ({ payload }) => {
                await timer(500).toPromise();

                TWISTERS.log('');
                console.log('');
                console.log(resolveFinishMessage(payload));
                return { success: !payload.err };
            }),
        ).toPromise();
    }
);
