import { virtualFs, workspaces } from '@angular-devkit/core';
import { SchematicContext, SchematicsException, Tree } from '@angular-devkit/schematics';

import init from '../commands/init';
import { NgxSshDeployNgAddSchematic } from '../schema';
import { packageJson } from '../utils';


function createHost(tree: Tree): workspaces.WorkspaceHost {
    return {
        async readFile(path: string): Promise<string> {
            const data = tree.read(path);
            if (!data) {
                throw new SchematicsException('File not found.');
            }
            return virtualFs.fileBufferToString(data);
        },
        async writeFile(path: string, data: string): Promise<void> {
            return tree.overwrite(path, data);
        },
        async isDirectory(path: string): Promise<boolean> {
            return !tree.exists(path) && tree.getDir(path).subfiles.length > 0;
        },
        async isFile(path: string): Promise<boolean> {
            return tree.exists(path);
        }
    };
}



export const ngAdd = (options: NgxSshDeployNgAddSchematic) => async (
    tree: Tree,
    _context: SchematicContext
) => {
    const host = createHost(tree);
    const { workspace } = await workspaces.readWorkspace('/', host);

    if (!options.project) {
        if (workspace.extensions.defaultProject) {
            options.project = workspace.extensions.defaultProject as string;
        } else {
            throw new SchematicsException(
                'No Angular project selected and no default project in the workspace'
            );
        }
    }

    const project = workspace.projects.get(options.project);
    if (!project) {
        throw new SchematicsException(
            'The specified Angular project is not defined in this workspace'
        );
    }

    if (project.extensions.projectType !== 'application') {
        throw new SchematicsException(
            `Deploy requires an Angular project type of "application" in angular.json`
        );
    }

    const { project: projectName, ...initOpts } = options;
    const { environments: {
        [projectName]: env
    } } = await init(initOpts, projectName);

    project.targets.add({
        name: 'deploy',
        builder: `${packageJson.name}:deploy`,
        options: env as any ?? {},
    });

    workspaces.writeWorkspace(workspace, host);
    return tree;
};
