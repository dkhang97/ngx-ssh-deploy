import { ObservedValueOf } from 'rxjs';
import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';

export function workerWrap<T extends Record<string, (...args: any[]) => any>>(filename: string, fnMap: T): {
    [K in keyof T]: (...args: Parameters<T[K]>) => Promise<ReturnType<T[K]> extends Promise<any> ? ObservedValueOf<ReturnType<T[K]>> : ReturnType<T[K]>>
} {
    if (!isMainThread) {
        (async () => {
            try {
                const [fnName, args] = JSON.parse(workerData);
                const fn = fnMap[fnName];
                const result = await Promise.resolve(fn.apply(fn, args));
                parentPort.postMessage(JSON.stringify({ result }));
            } catch (e) {
                parentPort.postMessage(JSON.stringify({ error: { message: e.message, stack: e.stack } }));
            }
        })();
    }

    const ret = {} as any;

    for (const [key, fn] of Object.entries(fnMap)) {
        ret[key] = async (...args) => {
            if (isMainThread) {
                const worker = new Worker(filename, { workerData: JSON.stringify([key, args]) });

                try {
                    return await new Promise((resolve, reject) => {
                        worker.once('message', msg => {
                            const resp = JSON.parse(msg);
                            if (resp.error) {
                                reject(Object.assign(new Error(resp.error.message), { stack: resp.error.stack }));
                            } else {
                                resolve(resp.result);
                            }
                        });
                        worker.once('error', err => reject(err));
                    });
                } finally {
                    worker.terminate();
                }
            } else {
                return Promise.resolve(fn.apply(fn, args));
            }
        };
    }

    return ret;
}