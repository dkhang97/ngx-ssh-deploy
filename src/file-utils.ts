
import fsExtra from 'fs-extra';
import fsPath from 'path';

export function* iterateFiles(path: string) {
    const stats = fsExtra.statSync(path);

    if (stats.isFile()) {
        yield path;
    } else if (stats.isDirectory()) {
        for (const fi of fsExtra.readdirSync(path)) {
            yield* iterateFiles(fsPath.join(path, fi));
        }
    }
}

export function ensureAbsolutePath(path: string) {
    return fsPath.isAbsolute(path) ? path : fsPath.resolve(path);
}
