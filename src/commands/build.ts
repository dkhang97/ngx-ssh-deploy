import { exec } from 'child_process';
import { blueBright, magenta, red } from 'cli-color';
import fsPath from 'path';
import { map, switchMap } from 'rxjs/operators';
import { promisify } from 'util';

import angularConfig from '../locals/angular';
import { promptSingleEnvironment, renderCommand } from '../locals/ngx-ssh-deploy';
import { ProgressLogger } from '../log-progress';
import { prompt2 } from '../utils';
import buildProject from './deploy/build-project';

export = async function build({ environment, outputPath, architect }) {
    const ngCfg = await angularConfig;
    if (!ngCfg) {
        console.log(red(`${fsPath.resolve('./angular.json')} is not existed!`));
        return;
    }

    const logger = new ProgressLogger();

    await promptSingleEnvironment(environment).pipe(
        switchMap(async opt => {
            const architects = Object.keys(opt.buildCommand);

            if (architects.includes(architect)) {
                return { opt, architect: architect as string };
            }

            return ({
                opt, architect: architects.length < 2 ? architects[0] : await prompt2<string>('Select architect', {
                    type: 'select', choices: architects.map(value => ({ title: value, value })),
                })
            });
        }),
        map(({ opt, architect }) => {
            const prjName = opt.env.ngProject || ngCfg.defaultProject;
            const project = ngCfg.projects[prjName];

            let dirDist = outputPath ?? project.architect[architect]?.options.outputPath;
            if (dirDist && !fsPath.isAbsolute(dirDist)) {
                dirDist = fsPath.resolve(dirDist);
            }
            return { opt, dirDist, architect, project };
        }),
        switchMap(v => {
            const buildCommand = [...v.opt.localBuildCommand[v.architect], '--output-path', v.dirDist];

            return logger.log('Building project ' + magenta(buildCommand.join(' ')),
                buildProject(buildCommand.slice(1), {
                    nodeOptions: Object.assign({}, v.opt.env.nodeOptions)
                })).pipe(map(() => v));
        }),
        switchMap(async ({ dirDist, opt, architect, project }) => {

            const commands = opt.env.postBuildCommand?.[architect]?.map(cmd => renderCommand(cmd, {
                project, deployOptions: opt, dist: dirDist,
            }));

            if (Array.isArray(commands)) {
                for (let idx = 0; idx < commands.length; idx++) {
                    const cmd = commands[idx];
                    await logger.log([`Post build (${idx + 1}/${commands.length})`,
                    blueBright(`[${opt.name}]`), magenta(cmd)].join(' '), async () => {
                        await promisify(exec)(cmd);
                        // const [command, ...args] = cmd.split(' ').map(c => c.trim()).filter(c => !!c);
                        // return await promisify(spawn)(command, args, {});
                    }).toPromise();
                }
            }

        })
    ).toPromise();

    await new Promise(resolve => setTimeout(resolve, 100));
}
