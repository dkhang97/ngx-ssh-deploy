import { execSync } from 'child_process';
import { green, inverse, red } from 'cli-color';
import fsExtra from 'fs-extra';
import { nanoid } from 'nanoid';
import fsPath from 'path';

import { promptSingleEnvironment } from '../locals/ngx-ssh-deploy';
import { prompt2, tempAppDir } from '../utils';

export = async function updateTargetLogin({ environment, file }) {
    const env = await promptSingleEnvironment(environment).toPromise();

    if (!env) {
        return;
    }

    const endpoint = env?.env?.endpoint;
    const pemFile = endpoint?.pemFile;

    if (!pemFile) {
        console.log(red(`PEM File is not specified`));
        return;
    }

    const publicKey = await (async () => {
        const tmpCertPath = fsPath.join(await tempAppDir.toPromise(), 'certs', nanoid() + '.pem');
        try {
            if (fsExtra.existsSync(pemFile)) {
                fsExtra.copySync(pemFile, tmpCertPath);
                fsExtra.chmodSync(tmpCertPath, 0o400)
                return execSync(`ssh-keygen -y -f "${tmpCertPath}"`).toString().trim();
            } else {
                execSync(`ssh-keygen -q -t rsa -b 4096 -m PEM -N "" -f "${tmpCertPath}"`);
                fsExtra.copySync(tmpCertPath, pemFile);

                const pubFile = tmpCertPath + '.pub';

                try {
                    return fsExtra.readFileSync(pubFile)
                        .toString('utf-8').trim();
                } finally {
                    fsExtra.removeSync(pubFile);
                }
            }
        } finally {
            fsExtra.removeSync(tmpCertPath);
        }
    })();

    console.log();
    console.log();

    const shellScripts = [
        `ngx_deploy_user="${endpoint.user}"`,
        `[[ ! -d "/home/$ngx_deploy_user" ]] && sudo useradd -p "" -m "$ngx_deploy_user" -s /bin/bash`,
        `sudo -u "$ngx_deploy_user" mkdir -p "/home/$ngx_deploy_user/.ssh"`,
        `sudo -u "$ngx_deploy_user" sh -c "echo \\"${publicKey}\\" >> \\"/home/$ngx_deploy_user/.ssh/authorized_keys\\""`,
    ];

    if (file) {
        if (fsExtra.existsSync(file)) {
            const ow: boolean = await prompt2(`Overwrite '${file}'?`, { type: 'confirm' });
            if (!ow) return;
        }

        fsExtra.writeFileSync(file, Buffer.from([
            '#!/bin/bash',
            '',
            ...shellScripts,
            '',
        ].join('\n')), { flag: 'w' });
        console.log(`Wrote ${green(file)}`);
        const fileBaseName = fsPath.basename(file);
        console.log('To execute on target machine, copy the file then run: ' +
            inverse(`chmod +x "${fileBaseName}" && sudo ./${fileBaseName}`));
        console.log();
        return;
    }

    console.log('Run the following commands on target machine');
    console.log();
    shellScripts.forEach(s => console.log(inverse(s)));
    console.log();
}
