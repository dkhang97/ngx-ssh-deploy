import { magenta, red } from 'cli-color';
import fs from 'fs';
import fsPath, { resolve as resolvePath } from 'path';
import { defer } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { start as startAnalyzer, BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import { pick } from 'lodash-uni'
import angularConfig, { resolveAngularProjectOptions } from '../locals/angular';
import { promptSingleEnvironment } from '../locals/ngx-ssh-deploy';
import { ProgressLogger } from '../log-progress';
import { prompt2, readLocal, tmpDb } from '../utils';
import buildProject, {
    computeNgProjectSourcesHash,
    resolveNgProjectSources,
} from './deploy/build-project';
import { createIntegrityChecker } from './deploy/integrity-checker';

export = async function stats({ environment, forceBuild, architect }) {
    const ngCfg = await angularConfig;
    if (!ngCfg) {
        console.log(red(`${resolvePath('./angular.json')} is not existed!`));
        return;
    }

    await tmpDb.read();
    const logger = new ProgressLogger();

    await promptSingleEnvironment(environment).pipe(
        switchMap(async opt => {
            const architects = Object.keys(opt.buildCommand);

            if (architects.includes(architect)) {
                return { opt, architect: architect as string };
            }

            return ({
                opt, architect: architects.length < 2 ? architects[0] : await prompt2<string>('Select architect', {
                    type: 'select', choices: architects.map(value => ({ title: value, value })),
                })
            });
        }),
        map(({ opt, architect }) => {
            const prjName = opt.env.ngProject || ngCfg.defaultProject;
            // const project = ngCfg.projects[prjName];

            // let dirDist = project.architect.build.options.outputPath;
            // if ('projectConfiguration' in opt.env) {
            //     dirDist = project.architect.build.configurations?.[opt.env.projectConfiguration]?.outputPath ?? dirDist;
            // }
            // dirDist = fsPath.resolve(dirDist);
            const dirDist = opt.buildPath[architect];
            return { opt, prjName, dirDist, architect, sourceStatChecksum: opt.buildCache?.sourceStatChecksum };
        }),
        switchMap(v => {
            const buildParam = v.opt.buildParameters[v.architect];
            const prjOpts = resolveAngularProjectOptions(v.architect as any,
                buildParam.project, buildParam.configuration);
            const prjSrc = defer(() => logger.log(`Reading project ${buildParam.project}:${buildParam.configuration}`,
                async () => prjOpts.then(o => resolveNgProjectSources(o, {
                    includeExtraFiles: false,
                    platform: v.architect === 'build' ? 'browser' : 'node',
                })))).pipe(shareReplay(1));
            const srcHash = defer(() => prjSrc.pipe(switchMap(src => computeNgProjectSourcesHash(Promise.resolve(src))))).pipe(shareReplay(1));

            const project = ngCfg.projects[v.prjName];

            const checker = createIntegrityChecker(['sourceStatChecksum', v.architect], () => srcHash.toPromise());

            const skipBuildObs = defer(async () => {
                if (!forceBuild && v.sourceStatChecksum && fs.existsSync(fsPath.join(v.dirDist, 'stats.json'))) {
                    return checker.checksumMatched(v.opt, project);
                }
            });

            const buildCommand = [...v.opt.buildCommand[v.architect], '--named-chunks', '--stats-json'];

            switch (v.architect) {
                case 'build':
                    buildCommand.push('--build-optimizer=false');
                    break;

                case 'server':
                    buildCommand.push('--optimization=false')
                    break;
            }

            return skipBuildObs.pipe(
                switchMap(async skipBuild => {
                    if (!skipBuild) {
                        await logger.log('Building project ' + magenta(buildCommand.join(' ')),
                            buildProject(buildCommand.slice(1), {
                                nodeOptions: Object.assign({}, v.opt.env.nodeOptions)
                            })).toPromise();

                        await checker.storeChecksum(v.opt, project)
                            .then(() => tmpDb.write()).catch(() => { });
                    }

                    return v;
                }),
            );
        }),
        switchMap(async ({ dirDist, prjName, opt, architect }) => {
            const bundleStats = await readLocal<object>(fsPath.join(dirDist, 'stats.json'),
                d => JSON.parse(d.toString()));

            const { configuration } = opt.buildParameters[architect];
            const cfgReport = [architect === 'build' ? '' : architect, configuration === 'production' ? '' : configuration].filter(p => !!p).join(':')

            await startAnalyzer(bundleStats, {
                ...pick(new BundleAnalyzerPlugin().opts || {}, 'analyzerUrl'),
                port: 0,
                reportTitle: `[WBA] ${prjName}${(cfg => cfg ? ` (${cfg})` : '')(cfgReport)} - ${new Date().toLocaleString()}`,
                bundleDir: dirDist,
            });
        })
    ).toPromise();

    await tmpDb.write();
    await new Promise(() => { });
}
