import { greenBright, red, yellow } from 'cli-color';
import { existsSync, writeFileSync } from 'fs';
import { normalize, relative, resolve } from 'path';

import { ensureAbsolutePath } from '../file-utils';
import angularConfig from '../locals/angular';
import deployConfig, { DeployConfig } from '../locals/ngx-ssh-deploy';
import { DeployEndpoint, NgxSshDeployNgAddSchematic } from '../schema';
import { packageJson, prompt2 } from '../utils';

export = async function init(opt: Omit<NgxSshDeployNgAddSchematic, 'project'>, ngAddProject?: string): Promise<DeployConfig> {
    const configFile = resolve('./ngx-ssh-deploy.json');

    if (!ngAddProject && await deployConfig) {
        console.log(yellow(`${configFile} is already existed!`));
        return { environments: {} };
    }

    if (!ngAddProject && !await angularConfig) {
        console.log(red(`${resolve('./angular.json')} is not existed!`));
        return { environments: {} };
    }

    let { endpoint, sshPath } = opt;
    const { pemFile } = opt;

    if (!endpoint) {
        endpoint = await prompt2('Endpoint?', { validate: v => !!v || '' });
    }

    let [user, host] = (endpoint || '').split('@');

    if (!host) {
        host = user
        user = ''
    }

    if (!user) {
        user = await prompt2('User?', { validate: v => !!v || '' });
    }

    const projectName = ngAddProject ?? ((await angularConfig).defaultProject || 'angular');

    let envEndpoint: DeployEndpoint = { host, user };

    if (pemFile) {
        let absFile = ensureAbsolutePath(pemFile);
        envEndpoint['pemFile'] = './' + normalize(relative(process.cwd(), absFile)).replace(/\\/g, '/');
    }

    if (!sshPath) {
        sshPath = `/home/${user}/${projectName}`;
    }

    let opts: DeployConfig = {
        environments: {
            [projectName]: {
                endpoint: envEndpoint,
                ssh: { path: sshPath }
            }
        }
    }

    const $schema = `./node_modules/${packageJson.name}/ngx-ssh-deploy.schema.json`;
    if (existsSync($schema)) {
        opts = {
            $schema,
            ...opts,
        }
    }

    if (!ngAddProject) {
        writeFileSync(configFile, JSON.stringify(opts, null, 2));
        console.log(`Created ${greenBright(configFile)}`);
    }

    return opts;
}