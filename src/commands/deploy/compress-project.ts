import fs from 'fs';
import glob from 'glob';
import JSZip from 'jszip';
import { join } from 'path';
import { defer, forkJoin, merge, Observable } from 'rxjs';
import { map, mapTo, mergeMap, shareReplay, tap, toArray } from 'rxjs/operators';
import temp from 'temp';
import fsExtra from 'fs-extra';
import { tempAppDir, toPromise } from '../../utils';


export default function compressProject({ dirDist, prjName }: { dirDist: string, prjName: string }) {
    return new Observable<string[]>(subscriber => {
        const gl = glob('**', { cwd: dirDist, nosort: true, mark: true }, (err, matches) => {
            if (err) {
                subscriber.error(err);
                return;
            }

            const files = matches.filter(f => !f.endsWith('/'));

            if (files.length === 0) {
                subscriber.error(new Error('Nothing to deploy!'));
            } else {
                subscriber.next(files);
            }
            subscriber.complete();
        });

        return { unsubscribe: () => gl.abort() };
    }).pipe(
        mergeMap(files => {
            const zip = new JSZip();

            return merge(...files.map(file => new Observable<Buffer>(subscriber => {
                const stream = fs.createReadStream(join(dirDist, file));
                stream.on('data', chunk => subscriber.next(chunk));
                stream.on('error', err => subscriber.error(err));
                stream.on('close', () => subscriber.complete());

                return { unsubscribe: () => stream.close() };
            }).pipe(
                toArray(),
                map(buffers => ({ file, buffer: Buffer.concat(buffers) })),
            ))).pipe(
                tap(({ file, buffer }) => zip.file(file, buffer)),
                toArray(),
                map(() => zip.generateNodeStream({
                    compression: 'DEFLATE',
                    compressionOptions: { level: 9 },
                    type: 'nodebuffer',
                })),
            );
        }),
        mergeMap(async zipStream => {
            const dir = await tempAppDir.pipe(map(d => join(d, 'compresses'))).toPromise();
            await fsExtra.ensureDir(dir);

            const { path, fd } = await toPromise<temp.OpenFile>(callback => temp.track().open({
                dir,
                prefix: prjName + '-',
                suffix: '.zip',
            }, callback));
            return { zipStream, path, fd };
        }),
        mergeMap(({ zipStream, path, fd }) => new Observable<string>(subscriber => {
            const stream = fs.createWriteStream(null, { fd, autoClose: true });
            zipStream.pipe(stream)
                .on('finish', () => {
                    subscriber.next(path);
                    subscriber.complete();
                }).on('error', e => subscriber.error(e))
            return { unsubscribe: () => stream.close() };
        })),
    );
}
