import fsExtra from 'fs-extra';
import { isEqual, uniq } from 'lodash-uni';
import fsPath from 'path';

import { computeHashWorker, pathChecksum } from '../../hash';
import { NgProject } from '../../locals/angular';
import { DeployOpts } from '../../locals/ngx-ssh-deploy';
import { ProjectBuildCache, exceptionWriter, forceStringArray } from '../../utils';

export function createIntegrityChecker<K extends keyof ProjectBuildCache>(
    cacheKeyAndArch: K | [K, string], checksumResolver: (opt: DeployOpts, project: NgProject) => (ProjectBuildCache[K] | Promise<ProjectBuildCache[K]>),
    comparator: (resolvedVal: ProjectBuildCache[K], cachedVal: ProjectBuildCache[K]) => (boolean | Promise<boolean>) = (a, b) => a === b) {

    let cacheKey: string;

    if (Array.isArray(cacheKeyAndArch)) {
        let arch: string;
        [cacheKey, arch] = cacheKeyAndArch;

        if (arch && arch !== 'build') {
            cacheKey += '_' + arch;
        }
    } else {
        cacheKey = cacheKeyAndArch;
    }

    return {
        async checksumMatched(opt: DeployOpts, project: NgProject) {
            try {
                const cachedSum = opt.buildCache?.[cacheKey];
                const resolvedSum = await Promise.resolve(checksumResolver(opt, project));

                return await Promise.resolve(comparator(resolvedSum, cachedSum));
            } catch {
                return false;
            }
        },
        async storeChecksum(opt: DeployOpts, project: NgProject) {
            try {
                opt.buildCache[cacheKey] = await Promise.resolve(checksumResolver(opt, project));
            } catch (err) {
                exceptionWriter.subscribe(e => e.write(err, `Failed to store build checksum for [${opt.name}]/${cacheKey}`));
            }
        },
    };
}

export const buildChecksumIntegrityChecker = (architect: string) =>
    createIntegrityChecker(['buildChecksum', architect], opt => pathChecksum(opt.buildPath[architect]));

export const sourceChecksumIntegrityCheckers = [
    createIntegrityChecker('sourceChecksum', (_, project) => pathChecksum(fsPath.resolve(project.sourceRoot))),
    createIntegrityChecker('libChecksum', async opt => {
        const libSumTasks = uniq(forceStringArray(opt.env.libraryPath))
            .map(async path => ({ [path]: await pathChecksum(path) }));

        return Object.assign({}, ...await Promise.all(libSumTasks));
    }, (resolved, cached) => !Object.keys(resolved).length || isEqual(resolved, cached),
    ),
    createIntegrityChecker('packageJsonChecksum', async () => {
        const pkgFiles = ['package.json', 'package-lock.json', 'yarn.lock', 'pnpm-lock.yaml', '.npmrc'];
        // const { hash } = await hashElement(fsPath.resolve('.'), {
        //     files: { include: pkgFiles },
        // });
        const hash = await computeHashWorker(process.cwd(), pkgFiles);

        return hash;
    }),
    createIntegrityChecker('nodeModulesChecksum', async opt => {
        const appPackageJson: { dependencies: Record<string, string> } = await fsExtra.readJSON(fsPath.join(opt.buildCache.source.path, 'package.json'));
        const depPaths = Object.keys(appPackageJson?.dependencies || {});
        const nodeModPath = fsPath.resolve('node_modules');

        // const { hash } = await hashElement(nodeModPath, {
        //     folders: { include: depPaths },
        // });
        const hash = await computeHashWorker(nodeModPath, depPaths);

        return hash;
    }),

]

export const buildIntegrityCheckers = (architect: string) => [
    buildChecksumIntegrityChecker(architect),
    ...sourceChecksumIntegrityCheckers,
];
