import { BuildOptions, build } from 'esbuild';
import fsExtra from 'fs-extra';
import { sortBy } from 'lodash-uni';
import mod from 'module';
import fsPath from 'path';
import * as morph from 'ts-morph';

import { ensureAbsolutePath } from '../../file-utils';
import { workerWrap } from '../../worker';

export async function resolveProjectSources(rootFile: string, opts?: {
    fileReplacements?: Record<string, string>,
    tsConfigFilePath?: string,
    extraSourceFiles?: string[],
    includeResources?: boolean,
    platform?: BuildOptions['platform'],
}) {
    const replacedFileName = (resFile: string) => {
        if (typeof resFile === 'string') {
            resFile = ensureAbsolutePath(resFile).replace(/\\/g, '/');

            for (const [key, val] of Object.entries(opts?.fileReplacements ?? {})) {
                if (resFile === ensureAbsolutePath(key).replace(/\\/g, '/')) {
                    return ensureAbsolutePath(val).replace(/\\/g, '/');
                }
            }
        }

        return resFile;
    };

    const nodeModSet = new Set<string>();

    const prj = new morph.Project({
        tsConfigFilePath: opts?.tsConfigFilePath,
        skipFileDependencyResolution: true,
        compilerOptions: {
            skipDefaultLibCheck: true,
            skipLibCheck: true,
            sourceMap: false,
            declarations: false,
        },
        resolutionHost: host => {
            const cwd = host.getCurrentDirectory();
            const fileNameCs = (typeof host.useCaseSensitiveFileNames === 'function' ? host.useCaseSensitiveFileNames() : host.useCaseSensitiveFileNames) ?? true;

            const normalizeFileName = (fileName: string) => {
                if (typeof fileName === 'string') {
                    if (fileNameCs) {
                        return fileName;
                    }

                    return fileName.toLowerCase();
                }

                return fileName;
            }

            const modCache = morph.ts.createModuleResolutionCache(cwd, normalizeFileName);

            return {
                resolveModuleNames: (moduleNames, file, _, rRef, opts) => {
                    const mod = [];

                    for (const modName of moduleNames) {

                        const { resolvedModule, ...rest } = morph.ts.nodeModuleNameResolver(modName, file, opts, host, modCache, rRef);

                        if (resolvedModule) {
                            const isModFromRel = /^\.\.?[\\/]/.test(modName);

                            if (!isModFromRel && resolvedModule.isExternalLibraryImport) {
                                nodeModSet.add(modName);
                            }

                            if (isModFromRel) {
                                for (const [key, val] of Object.entries(opts?.fileReplacements ?? {})) {
                                    if (normalizeFileName(resolvedModule.resolvedFileName) === normalizeFileName(ensureAbsolutePath(key).replace(/\\/g, '/'))) {
                                        resolvedModule.resolvedFileName = ensureAbsolutePath(val).replace(/\\/g, '/');
                                        break;
                                    }
                                }
                            } else {
                                try {
                                    const { packageId } = resolvedModule;
                                    let pkgDir = fsPath.join(cwd, 'node_modules', (typeof packageId === 'object' ? packageId?.name : packageId) ?? modName);

                                    let pkg;
                                    try {
                                        pkg = fsExtra.readJSONSync(fsPath.join(pkgDir, 'package.json'));
                                    } catch {
                                        for (const loc of (rest['affectingLocations'] || [])) {
                                            try {
                                                pkg = fsExtra.readJSONSync(loc);
                                                pkgDir = fsPath.dirname(loc);
                                                break;
                                            } catch { }
                                        }
                                    }

                                    if (pkg) {
                                        for (const buildType of ['fesm2020', 'fesm2015']) {
                                            if (pkg[buildType]) {
                                                const buildFile = fsPath.join(pkgDir, pkg[buildType]);

                                                if (fsExtra.existsSync(buildFile)) {
                                                    resolvedModule.resolvedFileName = buildFile;
                                                    break;
                                                }
                                            }

                                        }
                                    }

                                } catch { }
                            }
                        }

                        mod.push(resolvedModule);
                    }

                    return mod;
                }
            };
        }
    });

    const esNodeSet = new Set<string>();
    const esSourceSet = new Set<string>();

    for (const file of opts?.extraSourceFiles || []) {
        if (file) {
            esSourceSet.add(replacedFileName(file));
        }
    }

    esSourceSet.add(ensureAbsolutePath(rootFile).replace(/\\/g, '/'));

    await build({
        entryPoints: [rootFile],
        write: false,
        sourcemap: false,
        bundle: true,
        platform: opts?.platform || 'browser',
        logLevel: 'silent',
        tsconfig: opts?.tsConfigFilePath,
        plugins: [{
            name: 'ngx-deploy-resolve',
            setup(build) {

                if (opts?.includeResources ?? true) {
                    build.onLoad({ filter: /\.ts$/i, namespace: 'file' }, (args) => {
                        if (!isFromNodeModule(args.path)) {
                            const sf = prj.addSourceFileAtPath(args.path);

                            const isComponent = (node: morph.Expression) => {
                                for (const imp of sf.getImportDeclarations()) {
                                    if (imp.getModuleSpecifierValue() === '@angular/core') {
                                        if (node instanceof morph.Identifier) {
                                            for (const spec of imp.getNamedImports()) {
                                                if (spec.getName() === 'Component' && node.getText() === (spec.getAliasNode()?.getText() ?? spec.getName())) {
                                                    return true;
                                                }
                                            }
                                        } else if (node instanceof morph.PropertyAccessExpression && node.getName() === 'Component') {
                                            const spec = imp.getNamespaceImport() ?? imp.getDefaultImport();
                                            if (spec?.getText() === node.getExpression().getText()) { return true; }
                                        }
                                    }
                                }
                            };

                            const absoluteResource = (path: string) => fsPath.join(fsPath.dirname(sf.getFilePath()), path).replace(/\\/g, '/');

                            for (const cls of sf.getClasses()) {
                                const compDec = cls.getDecorators().find(d => isComponent(d.getCallExpression().getExpression()));
                                const [arg] = compDec?.getArguments() ?? [];

                                if (arg instanceof morph.ObjectLiteralExpression) {
                                    const templateUrl = arg.getProperty('templateUrl')
                                        ?.asKind(morph.SyntaxKind.PropertyAssignment)
                                        ?.getInitializerIfKind(morph.SyntaxKind.StringLiteral)?.getLiteralValue();

                                    if (templateUrl) {
                                        esSourceSet.add(absoluteResource(templateUrl));
                                    }

                                    const styleUrls = arg.getProperty('styleUrls')
                                        ?.asKind(morph.SyntaxKind.PropertyAssignment)
                                        ?.getInitializerIfKind(morph.SyntaxKind.ArrayLiteralExpression)
                                        ?.getElements?.().map(el => el.asKind(morph.SyntaxKind.StringLiteral)?.getLiteralValue()) || [];

                                    for (const url of styleUrls) {
                                        if (url) {
                                            esSourceSet.add(absoluteResource(url));
                                        }
                                    }
                                }
                            }

                            return { contents: sf.getText(), loader: 'ts' };
                        }
                        return null;
                    });
                }

                build.onResolve({ filter: /.*/, namespace: 'file' }, async (args) => {
                    if (mod.builtinModules.includes(args.path)) {
                        return;
                    }

                    const isRelPath = /^\.\.?[\\\/]/.test(args.path);

                    if (!isRelPath && !esNodeSet.has(args.path)) {
                        const pathStats = await fsExtra.stat(fsPath.join('node_modules', args.path, 'package.json')).catch(() => null as never);
                        if (pathStats?.isFile()) {
                            esNodeSet.add(args.path);
                        }
                    }

                    if (args.importer && !isFromNodeModule(args.importer)) {
                        const resEs = await build.resolve(args.path, { importer: args.importer, resolveDir: args.resolveDir, kind: 'import-statement' });
                        let resFile = resEs?.path;

                        if (!resFile) { return; }
                        resFile = resFile.replace(/\\/g, '/');

                        if (!isFromNodeModule(resFile)) {
                            if (isRelPath) {
                                resFile = replacedFileName(resFile);
                            }

                            esSourceSet.add(resFile);
                        }
                        return { path: resFile };
                    }

                    return null;
                });
            }
        }]
    });
    // prj.addSourceFileAtPath(rootFile);
    // prj.resolveSourceFileDependencies();

    return {
        // nodeModules: Array.from(nodeModSet),
        // sourceFiles: prj.getSourceFiles().map(sf => sf.getFilePath()),
        nodeModules: sortBy(Array.from(esNodeSet)),
        sourceFiles: sortBy(Array.from(esSourceSet)),
    };
}

export const worker = workerWrap(__filename, { resolveProjectSources });

export const resolveProjectSourcesWorker = worker.resolveProjectSources;

function isFromNodeModule(path: string) {
    return !fsPath.relative(fsPath.resolve('node_modules'), path).startsWith('..');
}