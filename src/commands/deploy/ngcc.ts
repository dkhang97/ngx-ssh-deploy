import fsExtra from 'fs-extra';
import fsPath from 'path';
import { ReplaySubject, defer } from 'rxjs';
import { filter, groupBy, map, mergeAll, shareReplay, switchMap, take } from 'rxjs/operators';

const ngccPkg = new ReplaySubject<{ projectDir: string, pkg: string, tsConfigPath?: string }>();
const ngccPkgCompiled = ngccPkg.pipe(
    groupBy(({ projectDir, pkg }) => JSON.stringify({ projectDir, pkg }),
        undefined, undefined, () => new ReplaySubject() as typeof ngccPkg),
    map(obs => obs.pipe(
        take(1),
        switchMap(seg => defer(async () => {
            const impNgcc = await import('./ngcc-register').catch(() => null as never);
            if (!impNgcc) {
                return seg;
            }

            const { projectDir, pkg, tsConfigPath } = seg
            const nodeModPath = fsPath.join(projectDir, 'node_modules');
            const targetPath = fsPath.join(nodeModPath, pkg);

            const pkgStat = await fsExtra.stat(fsPath.join(targetPath, 'package.json'))
                .catch(() => null as never);

            if (pkgStat?.isFile()) {
                impNgcc.mainNgcc({
                    // async: true,
                    basePath: nodeModPath,
                    targetEntryPointPath: targetPath,
                    compileAllFormats: false,
                    typingsOnly: true,
                    propertiesToConsider: ['es2015', 'browser', 'module', 'main'],
                    createNewEntryPointFormats: true,
                    tsConfigPath,
                });
            }

            return seg;
        })),
        shareReplay(1),
    )),
    mergeAll(2),
    shareReplay(),
);

ngccPkgCompiled.subscribe();

export async function ensureNgcComplied(projectDir: string, pkg: string, tsConfigPath?: string) {
    ngccPkg.next({ projectDir, pkg, tsConfigPath });

    return await ngccPkgCompiled.pipe(
        filter(e => e.projectDir === projectDir && e.pkg === pkg),
        take(1),
    ).toPromise();
}
