import './sm/deploy-effects';

import { EMPTY, forkJoin, from, of, throwError, timer } from 'rxjs';
import { concatMap, shareReplay, switchMap, toArray } from 'rxjs/operators';

import jsonTask, { getDeployOpts, renderEnvironmentTitle } from '../../locals/ngx-ssh-deploy';
import { ProgressLogger } from '../../log-progress';
import { TWISTERS } from '../../spinner';
import { forceStringArray, prompt2, tmpDb } from '../../utils';
import { createSshPrompt } from './connect-ssh';
import { DeployAction, SetLoggerAction, stama } from './sm/deploy-actions';
import { setDefaultBuildConcurrency } from './sm/deploy-effects/build-project';
import { createFinishPromise, resolveFinishMessage } from './utils';

interface DeployParams {
    environment: string;
    pwd: boolean;
    build?: boolean;
    buildVerbose?: boolean;
    buildConcurrency?: number;
}

export = async function deploy({ environment, build, buildVerbose, pwd, buildConcurrency }: DeployParams) {
    await tmpDb.read();

    const deployOptObs = from(jsonTask).pipe(
        switchMap(({ environments, defaultEnvironment }) => {
            let envNames = Object.keys(environments);

            if (envNames.length) {
                if (environment === '!' || (!environment && !defaultEnvironment)) {
                    if (envNames.length === 1) {
                        return from(envNames);
                    }

                    return forkJoin(envNames.map(n => getDeployOpts(n))).pipe(
                        switchMap(envs => prompt2<string[]>('Select deploy environment', {
                            type: 'multiselect', choices: envs.filter(e => e.env.display !== 'hidden').map(e => ({
                                title: renderEnvironmentTitle(e),
                                value: e,
                                disabled: e.env.display === 'disabled',
                                selected: e.env.display === 'selected',
                            }))
                        })),
                        switchMap(v => !v ? EMPTY : from(v)),
                    );
                }

                if (environment ?? false) {
                    const argEnvs: string[] = environment.split(',');
                    envNames = envNames.filter(e => argEnvs.includes(e));
                } else if (defaultEnvironment) {
                    const defEnv = forceStringArray(defaultEnvironment);
                    envNames = envNames.filter(e => defEnv.includes(e));
                }
            }

            return envNames.length ? from(envNames)
                : throwError(new Error('No environment selected'));
        }),
        concatMap(e => typeof e === 'string' ? getDeployOpts(e) : of(e)),
        shareReplay(),
    );

    setDefaultBuildConcurrency(buildConcurrency);

    const sshFinishedObs = createSshPrompt(deployOptObs, pwd);

    const logger = new ProgressLogger(sshFinishedObs);
    SetLoggerAction.dispatch({ logger });

    const actionsFinished = createFinishPromise();


    deployOptObs.pipe(toArray()).subscribe(async opts => {
        await sshFinishedObs.toPromise();
        opts.forEach(opt =>
            DeployAction.dispatch({ opt, build, buildVerbose })
        );
        stama.complete(DeployAction);
    });

    await stama.untilFinished();
    await timer(500).toPromise();

    await actionsFinished.then(actions => {
        TWISTERS.log('');
        console.log('');

        actions.forEach(({ payload }) => {
            console.log(resolveFinishMessage(payload));
        });
    });
}
