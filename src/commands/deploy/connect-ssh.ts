import { Observable, pipe } from 'rxjs';
import { concatMap, map, shareReplay, take, tap, toArray } from 'rxjs/operators';

import { yellow } from 'cli-color';
import { readFile } from 'fs';
import { Config as SshConfig } from 'node-ssh';
import fsPath from 'path';

import { prompt2, toPromise } from '../../utils';
import { groupBySshHost } from './utils';
import { DeployOpts } from '../../locals/ngx-ssh-deploy';
import { AssignSSHConfigAction } from './sm/deploy-actions';

export function pipeResolveSshConfig(requirePassword?: boolean) {
    return pipe(
        groupBySshHost(),
        concatMap(obs => obs.pipe(take(1), map(opt => ({ name: obs.key, opt })))),
        concatMap(async ({ name, opt }) => {

            const connectObj: SshConfig = {
                host: opt.sshEndPoint,
                username: opt.env.endpoint.user,
            };

            const { pemFile, port } = opt.env.endpoint;

            if (port !== undefined) {
                connectObj.port = port;
            }

            const key = await toPromise<Buffer>(callback =>
                readFile(pemFile, callback), true);

            const passwordPrompt = requirePassword ?? opt.env.ssh.passwordPrompt;

            if (key) {
                connectObj.privateKey = key.toString();

                if (passwordPrompt) {
                    connectObj.passphrase = await prompt2(`${yellow(name)} ${fsPath.basename(pemFile)} passphrase`, {
                        type: 'password',
                    });
                }
            } else if (passwordPrompt) {
                connectObj.password = await prompt2(yellow(name) + ' password', {
                    type: 'password',
                });
            }

            return { name, config: connectObj };
        }),
    );
}

export function createSshPrompt(deployOptObs: Observable<DeployOpts>, requirePassword?: boolean) {
    const sshPromptObs = deployOptObs.pipe(
        pipeResolveSshConfig(requirePassword),
        tap(({ name, config }) => AssignSSHConfigAction.dispatch({ name, config })),
        shareReplay(),
    );

    return sshPromptObs.pipe(
        toArray(),
        take(1),
    );
}
