import { spawn } from 'child_process';
import { BuildOptions } from 'esbuild';
import { snakeCase } from 'lodash-uni';
import fsPath, { resolve as resolvePath } from 'path';
import { Observable, ReplaySubject, defer } from 'rxjs';
import { map, shareReplay, toArray } from 'rxjs/operators';

import { computeHashWorker } from '../../hash';
import { NgProjectOptions, iterateNgAssets } from '../../locals/angular';
import { DeployEnvironment } from '../../schema';
import { exitSignal } from '../../utils';
import { resolveProjectSourcesWorker } from './source-resolver';

export interface BuildProjectOptions {
    buildVerbose?: boolean;
    nodeOptions?: DeployEnvironment['nodeOptions'];
}

export default function buildProject(cmd: string[], { buildVerbose, nodeOptions }: BuildProjectOptions = {}) {
    const obs = defer(() => new Observable<number>(subscriber => {
        const proc = spawn('node', [
            ...Object.entries(nodeOptions || {}).map(([key, val]) => `--${snakeCase(key)}=${new String(val)}`),
            resolvePath('./node_modules/@angular/cli/bin/ng'),
            ...cmd, ...(buildVerbose ? [] : ['--progress', 'false']),
        ], { detached: buildVerbose, shell: buildVerbose });

        const errSbj = new ReplaySubject<Buffer>();
        proc.stderr.on('data', (chunk: Buffer) => errSbj.next(chunk))

        proc.on('close', async code => {
            errSbj.complete();

            if (code) {
                const errMsg = await errSbj.pipe(toArray(), map(bs => Buffer.concat(bs).toString())).toPromise();
                subscriber.error(new Error(errMsg));
            } else {
                subscriber.next();
            }
            subscriber.complete();
        });

        exitSignal(() => proc.kill());

        return {
            unsubscribe: () => proc.kill(),
        };
    })).pipe(shareReplay());

    return obs;
}

export async function resolveNgProjectSources(prjOpts: NgProjectOptions, opts?: {
    includeExtraFiles?: boolean,
    platform?: BuildOptions['platform'],
}) {
    const includeExtraFiles = opts?.includeExtraFiles ?? true;

    return prjOpts?.main && resolveProjectSourcesWorker(prjOpts.main, {
        platform: opts?.platform,
        tsConfigFilePath: prjOpts?.tsConfig,
        fileReplacements: Object.assign({}, ...(prjOpts.fileReplacements?.map(e => ({
            [e.replace ?? e.src]: e.with ?? e.replaceWith,
        })) ?? [])),
        includeResources: includeExtraFiles,
        extraSourceFiles: includeExtraFiles && [
            prjOpts?.index,
            prjOpts?.polyfills,
            ...iterateNgAssets(prjOpts?.assets),
            ...iterateNgAssets(prjOpts?.scripts),
            ...iterateNgAssets(prjOpts?.styles),
        ],
    });
}

export async function computeNgProjectSourcesHash(sources: ReturnType<typeof resolveNgProjectSources>) {
    const prjSrc = await sources;

    if (Array.isArray(prjSrc?.sourceFiles) && prjSrc.sourceFiles.length) {
        const includeFiles = prjSrc.sourceFiles.map(f => fsPath.relative(process.cwd(), f).replace(/\\/g, '/'));
        // return await hashElement(process.cwd(), { files: { include: includeFiles, matchPath: true } }).then(e => e.hash);
        const hash = await computeHashWorker(process.cwd(), includeFiles);
        return hash;
    }
}