import { blueBright, green, red } from 'cli-color';
import { uniq } from 'lodash-uni';
import logSymbols from 'log-symbols';
import { EMPTY, from, Observable, pipe, ReplaySubject } from 'rxjs';
import { catchError, concatMap, groupBy, map, mergeScan, scan, share, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { ActionType } from 'stama-rx';

import { FinishAction, stama } from './sm/deploy-actions';

import type { DeployOpts } from '../../locals/ngx-ssh-deploy';
export function resolveSshHost({ sshEndPoint, env: { endpoint: { user } } }: DeployOpts) {
    return [user, sshEndPoint].map(v => v?.trim()).filter(v => !!v).join('@');
}

export function groupBySshHost<T = DeployOpts>(optSelector: (value: T) => DeployOpts = (v => v as any)) {
    return pipe(
        groupBy((value: T) => resolveSshHost(optSelector(value))
            , null, null, () => new ReplaySubject<T>()),
    );
}

export function mergeAction<T, A>(dispatcher: (payload: T, extra: {
    envNames: Observable<string[]>,
}) => Observable<A>, optSelector: (payload: T) => DeployOpts) {
    const identicalOptSbj = new ReplaySubject<{ payload: T, opt: DeployOpts }>();

    return pipe(
        mergeScan((acc, payload: T) => {
            identicalOptSbj.next({ payload, opt: optSelector(payload) });

            if (acc) {
                return EMPTY;
            }

            const envNames = identicalOptSbj.pipe(
                scan((envAry, { opt: { name } }) => uniq([...envAry, name]), [] as string[]),
                shareReplay(1),
            );

            return dispatcher(payload, { envNames }).pipe(
                map(result => ({ result, envNames })),
            );
        }, null as { result: A, envNames: Observable<string[]> }),
        tap({
            complete() {
                identicalOptSbj.complete()
            },
            error(err) {
                identicalOptSbj.complete();
                identicalOptSbj.subscribe(({ opt }) =>
                    FinishAction.dispatch({ opt, err }));
            },
        }),
        catchError(() => EMPTY),
        concatMap(result => identicalOptSbj.pipe(
            map(({ opt, payload }) => ({ ...result, opt, payload })),
        )),
        share(),
    )
}


export function transformConfigNames(transformMessage: (cfgNames: string) => string, architect?: string) {
    return pipe(
        map((e: string[]) => blueBright(`[${e.join(', ')}]` + (architect ? `:${architect}` : ''))),
        map(transformMessage),
    );
}

export function createFinishPromise() {
    return new Promise<ActionType<typeof FinishAction>[]>((resolve, reject) => {
        const actions: ActionType<typeof FinishAction>[] = [];
        stama.onAction(FinishAction).pipe(
            takeUntil(from(stama.untilFinished())),
        ).subscribe(action => actions.push(action), reject, () => resolve(actions));
    });
}

export function resolveFinishMessage({ opt: { name, url }, err }: ActionType<typeof FinishAction>['payload']) {
    const envName = blueBright(`[${name}]`);
    return err ? red(logSymbols.error + ` Not deployed ${envName} ${url.href}`)
        : green(logSymbols.success + ` Deployed ${envName} ${url.href}`);
}