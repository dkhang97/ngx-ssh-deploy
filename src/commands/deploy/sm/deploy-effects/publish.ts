import { blueBright, cyan, greenBright, magenta } from 'cli-color';
import fsPath from 'path';
import { concat, defer, forkJoin, of } from 'rxjs';
import {
  catchError,
  map,
  mapTo,
  mergeMap,
  share,
  switchMap,
  toArray,
  withLatestFrom,
} from 'rxjs/operators';
import sanitize from 'sanitize-filename';

import { renderCommand } from '../../../../locals/ngx-ssh-deploy';
import { packageJson } from '../../../../utils';
import { FinishAction, PublishAction, stama } from '../deploy-actions';

export const publish$ = stama.createEffect(e => e.fromAction(PublishAction).pipe(
    withLatestFrom(e.state$),
    mergeMap(([{ opt, sourcePaths, ssh, exec, project }, { logger }]) => defer(() => {
        return concat(
            forkJoin(sourcePaths.map(({ architect, path: sourcePath }) => {
                const cfgName = blueBright(`[${opt.name}]` + (sourcePaths.length > 1 ? `:${architect}` : ''));

                if (opt.env.directDeploy) {
                    return logger.log(`Uploading ${cfgName} ${greenBright(sourcePath)}`,
                        () => ssh.putDirectory(sourcePath, opt.env.ssh.path[architect]));
                } else {
                    return logger.log(`Uploading ${cfgName} ${greenBright(sourcePath /* zip file */)}`, async () => {
                        const path = fsPath.basename(sourcePath); // zip file name

                        const tmpPath: string = await exec('mktemp', '-d', `/tmp/${sanitize(packageJson.name)}.XXXX`);
                        await ssh.putFile(sourcePath, fsPath.posix.join(tmpPath, path));

                        const fullDirDeploy = opt.env.ssh.path[architect];

                        return {
                            dirTemp: tmpPath, zipPath: path, exec, fullDirDeploy,
                            dirTempDist: fsPath.posix.join(tmpPath, fsPath.basename(fullDirDeploy)),
                        };
                    }).pipe(
                        switchMap(p => {
                            const tmpZip = fsPath.posix.join(p.dirTemp, p.zipPath);
                            return logger.log(`Decompressing ${cfgName} ${cyan(tmpZip)}`,
                                () => p.exec('unzip', tmpZip, '-d', p.dirTempDist)).pipe(mapTo(p));
                        }),
                        switchMap(({ fullDirDeploy, exec, dirTempDist }) =>
                            logger.log(`Patching ${cfgName} ${cyan(fullDirDeploy)}`, async () => {
                                if (opt.env.deleteOldBuild) {
                                    try { await exec('mv', fullDirDeploy, dirTempDist + '_OLD'); }
                                    catch { /* Old project not exist, nothing to catch here */ }
                                    await exec('mv', dirTempDist, fullDirDeploy);
                                } else {
                                    await exec('cp', '-RfT', dirTempDist, fullDirDeploy);
                                }
                            })
                        ),
                    );
                }
            })),
            defer(async () => {
                const commands = opt.env.postDeployCommand
                    .map(cmd => renderCommand(cmd, {
                        project, deployOptions: opt,
                    }));

                for (let idx = 0; idx < commands.length; idx++) {
                    const cmd = commands[idx];
                    await logger.log([`Post deploy (${idx + 1}/${commands.length})`,
                    blueBright(`[${opt.name}]`), magenta(cmd)].join(' '), async () => {
                        // const [command, ...args] = cmd.split(' ').map(c => c.trim()).filter(c => !!c);
                        // await exec(command, ...args);
                        await exec(cmd);
                    }).toPromise();
                }

            }),
        ).pipe(toArray());
    }).pipe(
        map(() => FinishAction.create({ opt })),
        catchError(err => of(FinishAction.create({ opt, err }))),
    )),
    share(),
));