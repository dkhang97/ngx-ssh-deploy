import { magenta } from 'cli-color';
import os from 'os';
import { Observable, ObservedValueOf, ReplaySubject, defer, forkJoin, of } from 'rxjs';
import {
    filter,
    groupBy,
    map,
    mergeAll,
    shareReplay,
    switchMap,
    take,
    toArray,
    withLatestFrom,
} from 'rxjs/operators';

import { resolveAngularProjectOptions } from '../../../../locals/angular';
import { disposableLogger } from '../../../../log-progress';
import { tmpDb } from '../../../../utils';
import buildProject, {
    computeNgProjectSourcesHash,
    resolveNgProjectSources,
} from '../../build-project';
import { buildChecksumIntegrityChecker, createIntegrityChecker } from '../../integrity-checker';
import { ensureNgcComplied } from '../../ngcc';
import { mergeAction, transformConfigNames } from '../../utils';
import { BuildProjectAction, PostBuildProjectAction, stama } from '../deploy-actions';

const memGb = os.totalmem() / 1024 / 1024 / 1024
const memConc = Math.round(memGb / 8);
const cpuConc = os.cpus().length;

export let DEFAULT_CONCURRENCES = Math.max(1, Math.min(cpuConc, memConc));

export function setDefaultBuildConcurrency(concurrency: number) {
    DEFAULT_CONCURRENCES = concurrency;
}

let globalBuildId = 0;

const globalBuildSbj = new ReplaySubject<Observable<number>>();

const globalBuildObs = globalBuildSbj.pipe(
    obs => defer(() => obs.pipe(mergeAll(DEFAULT_CONCURRENCES))),
    shareReplay(),
);



export const buildProject$ = stama.createEffect(e => e.fromAction(BuildProjectAction).pipe(
    groupBy(({ opt: { buildCache: { source: { path, configuration, project } } } }) =>
        JSON.stringify({ path, configuration, project })),
    map(obs => obs.pipe(
        withLatestFrom(e.state$),
        mergeAction(([{ opt, project, build, buildVerbose }, { logger }], { envNames }) => defer(async () => {
            const architects = Object.keys(opt.buildCommand);

            const resolveArchitectPrebuild = async (architect: string) => {

                const integrityCheckers: ReturnType<typeof createIntegrityChecker>[] = [];

                const buildParam = opt.buildParameters[architect];
                const prjOpts = await resolveAngularProjectOptions(architect as any,
                    buildParam.project, buildParam.configuration);
                const tsConfigFilePath = prjOpts?.tsConfig;

                const logMsgRead = envNames.pipe(
                    transformConfigNames(cfgNames => `Reading project ${cfgNames} ${buildParam.project}:${buildParam.configuration}`, architects.length > 1 && architect),
                );

                const prjSrc = await logger.log(logMsgRead, async () => resolveNgProjectSources(prjOpts, {
                    platform: architect === 'build' ? 'browser' : 'node',
                })).toPromise();

                if (Array.isArray(prjSrc?.nodeModules) && prjSrc.nodeModules.length) {
                    await Promise.all(prjSrc.nodeModules
                        .map(async pkg => ensureNgcComplied(process.cwd(), pkg, tsConfigFilePath)));
                }

                const srcHash = defer(async () => computeNgProjectSourcesHash(Promise.resolve(prjSrc))).pipe(shareReplay(1));

                integrityCheckers.push(buildChecksumIntegrityChecker(architect));

                if (build === undefined) {
                    // integrityCheckers.push(...sourceChecksumIntegrityCheckers);

                    if (prjSrc?.sourceFiles?.length) {
                        integrityCheckers.push(createIntegrityChecker(['sourceChecksum', architect], () => srcHash.toPromise()));
                    }
                }

                const logMsgVerify = envNames.pipe(
                    transformConfigNames(cfgNames => `Verifying integrity ${cfgNames}`, architects.length > 1 && architect),
                );

                return {
                    [architect]: {
                        integrityCheckers,
                        skipBuild: build ? false : await disposableLogger(logMsgVerify, async () => {
                            for (const checker of integrityCheckers) {
                                if (!(await checker.checksumMatched(opt, project))) {
                                    return false;
                                }
                            }

                            return true;
                        }),
                    },
                };
            };

            return Object.assign({}, ...await Promise.all(architects.map(resolveArchitectPrebuild))) as
                ObservedValueOf<ReturnType<typeof resolveArchitectPrebuild>>;
        }).pipe(
            switchMap(preBuild => {
                const architects = Object.keys(preBuild);

                return forkJoin(architects
                    .map(architect => {
                        if (preBuild[architect].skipBuild) {
                            return of(undefined);
                        }

                        const logMsg = envNames.pipe(
                            transformConfigNames(cfgNames => `Building project ${cfgNames} ${magenta(opt.buildCommand[architect].join(' '))}`, architects.length > 1 && architect),
                        );

                        const currentBuildId = globalBuildId++;

                        const buildObs = defer(() => logger.log(logMsg, buildProject(opt.buildCommand[architect].slice(1), {
                            buildVerbose, nodeOptions: Object.assign({}, opt.env.nodeOptions),
                        }))).pipe(
                            toArray(),
                            map(() => currentBuildId),
                        );

                        globalBuildSbj.next(buildObs);

                        return globalBuildObs.pipe(
                            filter(id => id === currentBuildId),
                            take(1),
                            switchMap(() => {
                                const logMsg = envNames.pipe(
                                    transformConfigNames(cfgNames => `Storing build checksum ${cfgNames}`, architects.length > 1 && architect),
                                );

                                return disposableLogger(logMsg, async () => {
                                    await Promise.all(preBuild[architect].integrityCheckers.map(checker =>
                                        checker.storeChecksum(opt, project)));

                                    if (tmpDb.data?.postBuildDist) {
                                        delete tmpDb.data.postBuildDist[opt.buildCache.distPath[architect]];
                                    }

                                    await tmpDb.write();
                                });
                            }),
                        );
                    }))
            }),
            map(() => project),
        ), p => p[0].opt),
        map(({ opt, result }) => {
            return PostBuildProjectAction.create({
                opt, postBuildCommands: opt.env.postBuildCommand,
                project: result,
            });
        })
    )),
    mergeAll(),
), { signalCompleteOnFinished: PostBuildProjectAction });

