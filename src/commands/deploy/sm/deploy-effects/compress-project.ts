import { greenBright } from 'cli-color';
import { of, forkJoin } from 'rxjs';
import { groupBy, map, mergeAll, withLatestFrom } from 'rxjs/operators';

import compressProject from '../../compress-project';
import { mergeAction, transformConfigNames } from '../../utils';
import { CompressProjectAction, ConnectSSHAction, stama } from '../deploy-actions';


export const compressProject$ = stama.createEffect(e => e.fromAction(CompressProjectAction).pipe(
    groupBy(({ opt }) => opt.buildPath),
    map(obs => obs.pipe(
        withLatestFrom(e.state$),
        mergeAction(([{ opt: { name, buildPath, env, buildCommand }, project }, { logger }], { envNames }) => {
            if (env.directDeploy) {
                return of(undefined) as never;
            }

            const architects = Object.keys(buildCommand);
            return forkJoin(architects.map(architect => {
                const logMsg = envNames.pipe(transformConfigNames(cfgNames =>
                    `Compressing project ${cfgNames} ${greenBright(buildPath[architect])}`, architects.length > 1 && architect));

                return logger.log(logMsg, compressProject({ dirDist: buildPath[architect], prjName: name })).pipe(
                    map(path => ({ architect, path })),
                );
            })).pipe(
                map(zipPath => ({ zipPath, project })),
            );
        }, p => p[0].opt),
        map(({ opt, result: { zipPath, project } }) => ConnectSSHAction.create({ opt, zipPath, project })),
    )),
    mergeAll(),
), { signalCompleteOnFinished: ConnectSSHAction });