import { exec } from 'child_process';
import { blueBright, magenta } from 'cli-color';
import fsExtra from 'fs-extra';
import { nanoid } from 'nanoid';
import fsPath from 'path';
import { defer, of } from 'rxjs';
import { catchError, mergeMap, withLatestFrom } from 'rxjs/operators';
import { promisify } from 'util';

import { pathChecksum } from '../../../../hash';
import { renderCommand } from '../../../../locals/ngx-ssh-deploy';
import { tempAppDir, tmpDb } from '../../../../utils';
import {
  CompressProjectAction,
  FinishAction,
  PostBuildProjectAction,
  stama,
} from '../deploy-actions';

export const postBuildProject$ = stama.createEffect(e => e.fromAction(PostBuildProjectAction).pipe(
    withLatestFrom(e.state$),
    mergeMap(([{ opt, project, postBuildCommands }, { logger }]) => defer(async () => {

        const entries = Object.entries(postBuildCommands || {})
            .filter(([key]) => opt.buildCommand[key]);

        if (!entries.length) {
            return CompressProjectAction.create({ opt, project });
        }

        for (const [architect, postBuildCommand] of entries) {

            const postBuildDistMap = (tmpDb.data || (tmpDb.data = {})).postBuildDist = Object.assign({}, tmpDb.data.postBuildDist);
            const postBuildDist = postBuildDistMap[opt.buildCache.distPath[architect]] || (postBuildDistMap[opt.buildCache.distPath[architect]] = {});


            let postPath = postBuildDist.path;

            if (!postPath) {
                while (!postPath || Object.values(postBuildDistMap).some(b => b?.path === postPath)) {
                    postPath = nanoid();
                }
                postBuildDist.path = postPath;
            }

            const targetBuildPath = fsPath.join(await tempAppDir.toPromise(), 'post-builds', postPath);
            const sourcePath = opt.buildPath;
            opt = {
                ...opt, buildPath: {
                    ...sourcePath,
                    [architect]: targetBuildPath,
                }
            };

            const commands = postBuildCommand
                .map(cmd => renderCommand(cmd, {
                    project, deployOptions: opt,
                    dist: opt.buildPath[architect],
                }));

            if (!postBuildDist.commands || JSON.stringify(postBuildDist.commands) !== JSON.stringify(commands) ||
                !postBuildDist.checksum || (postBuildDist.checksum !== await pathChecksum(targetBuildPath))) {
                if (fsExtra.existsSync(targetBuildPath)) {
                    await fsExtra.remove(targetBuildPath);
                }
                await fsExtra.copy(sourcePath[architect], targetBuildPath, { recursive: true });

                for (let idx = 0; idx < commands.length; idx++) {
                    const cmd = commands[idx];
                    await logger.log([`Post build (${idx + 1}/${commands.length})`,
                    blueBright(`[${opt.name}]`), magenta(cmd)].join(' '), async () => {
                        await promisify(exec)(cmd);
                        // const [command, ...args] = cmd.split(' ').map(c => c.trim()).filter(c => !!c);
                        // return await promisify(spawn)(command, args, {});
                    }).toPromise();
                }

                postBuildDist.checksum = await pathChecksum(targetBuildPath);
                postBuildDist.commands = commands;
                await tmpDb.write();
            }
        }

        return CompressProjectAction.create({ opt, project });
    }).pipe(
        catchError(err => of(FinishAction.create({ opt, err }))),
    )),
), { signalCompleteOnFinished: CompressProjectAction });