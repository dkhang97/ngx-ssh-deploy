import { red } from 'cli-color';
import fsExtra from 'fs-extra';
import fsPath from 'path';
import { groupBy, map, mergeAll } from 'rxjs/operators';

import { AngularConfig } from '../../../../locals/angular';
import { withDeferred } from '../../../../utils';
import { BuildProjectAction, DeployAction, stama } from '../deploy-actions';

export const deploy$ = stama.createEffect(e => e.fromAction(DeployAction).pipe(
    groupBy(a => a.opt.buildCache.source.path),
    map(obs => obs.pipe(
        withDeferred(async () => {
            const jsonPath = fsPath.join(obs.key, 'angular.json');
            try {
                const json = await fsExtra.readJSON(jsonPath);
                return json as AngularConfig;
            } catch (e) {
                console.log(red(`${jsonPath} is not existed!`));
                throw e;
            }
        }),
        map(([{ opt, build, buildVerbose }, { projects }]) => BuildProjectAction.create({
            opt, build, buildVerbose,
            project: projects[opt.buildCache.source.project],
        })),
    )),
    mergeAll(),
), { signalCompleteOnFinished: BuildProjectAction });
