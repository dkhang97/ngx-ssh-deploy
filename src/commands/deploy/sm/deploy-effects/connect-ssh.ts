import { yellow } from 'cli-color';
import { NodeSSH } from 'node-ssh';
import { defer } from 'rxjs';
import { groupBy, map, mergeAll, withLatestFrom } from 'rxjs/operators';

import { mergeAction, resolveSshHost, transformConfigNames } from '../../utils';
import { ConnectSSHAction, PublishAction, stama } from '../deploy-actions';

export const connectSSH$ = stama.createEffect(e => e.fromAction(ConnectSSHAction).pipe(
    map(p => ({ ...p, sshHost: resolveSshHost(p.opt) })),
    groupBy(({ sshHost, opt: { env: { ssh: { su } } } }) => JSON.stringify({ sshHost, su })),
    map(obs => obs.pipe(
        withLatestFrom(e.state$),
        mergeAction(([{ sshHost, opt, project }, { logger, sshConfig }], { envNames }) =>
            logger.log(envNames.pipe(transformConfigNames(cfgNames => `Connecting to ${cfgNames} ${yellow(sshHost)}`)),
                defer(async () => {
                    const config = sshConfig[sshHost];
                    const ssh = new NodeSSH();
                    await ssh.connect(config);
                    const su = opt?.env?.ssh?.su;

                    return {
                        project, ssh,
                        exec: async (cmd: string, ...args: string[]) => {
                            if (!args.length) {
                                const fullCmd = (su ? 'sudo ' : '') + cmd;
                                const result = await ssh.execCommand(fullCmd, {
                                    // execOptions: { pty: { term: 'xterm' } },
                                });

                                if (result.stderr) {
                                    throw new Error(result.stderr);
                                }

                                return result.stdout;
                            }

                            return ssh.exec(su ? 'sudo' : cmd, su ? [cmd, ...args] : args, {
                                // execOptions: { pty: { term: 'xterm' } },
                            });
                        },
                    };
                })), p => p[0].opt),
        map(({ opt, result: { ssh, exec, project }, payload: [{ zipPath }] }) => PublishAction.create({
            ssh, exec, opt, project,
            sourcePaths: zipPath ?? Object.entries(opt?.buildPath || {})
                .map(([architect, path]) => ({ architect, path })),
        })),
    )),
    mergeAll(),
), { signalCompleteOnFinished: PublishAction });