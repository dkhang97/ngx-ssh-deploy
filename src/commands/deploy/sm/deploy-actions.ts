import StamaRX from 'stama-rx';

import { ProgressLogger } from '../../../log-progress';
import { exitSignal } from '../../../utils';

import type { NgProject } from '../../../locals/angular';
import type { DeployOpts } from '../../../locals/ngx-ssh-deploy';
import type { NodeSSH, Config as SshConfig } from 'node-ssh';
import { StandardizedDeployEnvironment } from '../../../standardizer';

export const stama = new StamaRX({
    logger: new ProgressLogger(),
    sshConfig: {} as Record<string, SshConfig>,
});

exitSignal(() => stama.destroy());

export const SetLoggerAction = stama.actionBuilder<{ logger: ProgressLogger }>((state, { logger }) =>
    ({ ...state, logger })).create('Set Logger');

export const AssignSSHConfigAction = stama.actionBuilder<{ name: string, config: SshConfig }>((state, { name, config }) =>
({
    ...state, sshConfig: {
        ...state.sshConfig, [name]: config,
    }
})).create('Assign SSH Config');

export const DeployAction = stama.actionBuilder<{
    opt: DeployOpts;
    build?: boolean;
    buildVerbose?: boolean;
}>().create('Deploy');

export const BuildProjectAction = stama.actionBuilder<{
    opt: DeployOpts;
    project: NgProject;
    build: boolean;
    buildVerbose: boolean;
}>().create('Build Project');

export const PostBuildProjectAction = stama.actionBuilder<{
    opt: DeployOpts;
    project: NgProject;
    postBuildCommands: StandardizedDeployEnvironment['postBuildCommand'];
}>().create('Post Build Project');

export const CompressProjectAction = stama.actionBuilder<{
    opt: DeployOpts;
    project: NgProject;
}>().create('Compress Project');

export const ConnectSSHAction = stama.actionBuilder<{
    opt: DeployOpts;
    project: NgProject;
    zipPath?: { architect: string, path: string }[]; // if direct, use opt.buildPath
}>().create('Connect SSH');

export const PublishAction = stama.actionBuilder<{
    opt: DeployOpts;
    ssh: NodeSSH;
    project: NgProject;
    exec(command: string, ...args: string[]): Promise<string>;
    sourcePaths: { architect: string, path: string }[];
}>().create('Publish Project');

export const FinishAction = stama.actionBuilder<{
    opt: DeployOpts;
    err?: Error; // undefined if no error
}>().create('Finish');
