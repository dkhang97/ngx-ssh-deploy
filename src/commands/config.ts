import { cyan, greenBright, inverse, red } from 'cli-color';
import { writeFileSync } from 'fs';
import { resolve, basename } from 'path';
import sanitize from 'sanitize-filename';

import { ensureAbsolutePath } from '../file-utils';
import { resolveAngularProjectOptions } from '../locals/angular';
import { promptSingleEnvironment } from '../locals/ngx-ssh-deploy';
import serverConfigs from '../server-configs';

export = async function config({ serverName, environment, outputFile }: Record<string, string>) {
    const svr = serverConfigs[serverName];

    if (typeof svr !== 'function') {
        console.log(red(`Configuration for '${serverName}' not found!`));
        return;
    }

    const env = await promptSingleEnvironment(environment).toPromise();

    if (!env) {
        return;
    }


    serverName = sanitize(serverName);
    if (!outputFile) {
        outputFile = resolve(`./${sanitize(env.name)}.${serverName}.conf`);
    } else {
        outputFile = ensureAbsolutePath(outputFile);
    }

    const buildParam = env.buildParameters.build;
    const prjOpts = await resolveAngularProjectOptions('build', buildParam.project, buildParam.configuration);

    console.log('Build command is: ' + inverse(env.localBuildCommand['build'].join(' ')));


    let indexFile = prjOpts?.index;
    if (indexFile) {
        indexFile = basename(indexFile);
    }

    writeFileSync(outputFile, svr(env.env.ssh.path['build'], env.path, indexFile));
    console.log(`Wrote ${cyan(serverName)}'s for ${cyan(env.path)} to ${greenBright(outputFile)}`);
}