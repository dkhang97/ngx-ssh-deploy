const cfg: Record<string, (dir: string, path: string, indexFile?: string) => string> = {};
export = cfg;


const apacheOption = `
    Options FollowSymLinks
    AllowOverride None
    Require all granted
`.trim();

const apacheMod = `
    # SwUpdate hash mismatched issue, consider to enable pagespeed_module when serviceWorker is not enabled (ngsw.json not existed)
    <IfModule pagespeed_module>
        ModPagespeed Off
    </IfModule>
`.trim();

cfg.apache = (dir, path, indexFile = 'index.html') => path === '/'
    ? `<Directory ${dir}>
    ${apacheOption}

    RewriteEngine On
    # If an existing asset or directory is requested go to it as it is
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
    RewriteRule ^ - [L]

    # If the requested resource doesn't exist, use ${indexFile}
    RewriteRule ^ /${indexFile}

    ${apacheMod}
</Directory>`
    : `Alias ${path} ${dir}
    
<Directory ${dir}>
    ${apacheOption}

    RewriteEngine On
    RewriteBase ${path}/
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ ${indexFile} [L,QSA]

    ${apacheMod}
</Directory>`;

cfg.nginx = (dir, path, indexFile = 'index.html') => `    location ${path || '/'} {
        index "${indexFile}";
        ${(path || '/') === '/' ? 'root' : 'alias'} "${dir}";
        try_files $uri $uri/ "${path}/${indexFile}";
    }`;
