const S = require('fluent-json-schema').default;

const singleOrArrayString = S.anyOf([
    S.string(),
    S.array().items(S.string()),
]);

const deployEndpoint = S.object()
    .additionalProperties(false)
    .prop('host', S.string())
    .prop('port', S.number().minimum(1).maximum(65535))
    .prop('user', S.string())
    .prop('pemFile', S.string())
    .required(['host', 'user']);


const ngBuildDeployProjectConfiguration = S.object()
    .additionalProperties(false)
    .prop('architect', S.string())
    .prop('configuration', S.string())
    .required(['configuration']);

const ngBuildDeployOptions = S.object()
    .additionalProperties(false)
    .prop('endpoint', deployEndpoint)
    .prop('ssh', S.object()
        .additionalProperties(false)
        .prop('path', S.anyOf([
            S.string(),
            S.object().additionalProperties(S.object().raw({ type: 'string' }))
        ]))
        .prop('su', S.boolean())
        .prop('passwordPrompt', S.boolean())
        .required(['path'])
    )
    .prop('projectConfiguration', S.anyOf([
        S.string(),
        S.array().items(S.anyOf([
            S.string(),
            ngBuildDeployProjectConfiguration,
        ]))
    ])).description("`ng build --configuration=<projectConfiguration>` " +
        "- If not specified, **production** will be used")
    .prop('targetUrl', S.string())
    .prop('deleteOldBuild', S.boolean()).description("Delete build on target machine before patching new content, not working with `directDeploy`")
    .prop('directDeploy', S.boolean()).description("If set to true, upload files directly to target ssh path. " +
        "Otherwise, zip build, upload to target temp path, then unzip to target ssh path")
    .prop('postBuildCommand', S.anyOf([
        singleOrArrayString,
        S.object().additionalProperties(singleOrArrayString)
    ])).description('Command to run after build')
    .prop('postDeployCommand', singleOrArrayString).description('Command to run after deploy')
    .prop('libraryPath', singleOrArrayString).description("Additional path for build integrity determination")
    .prop('display', S.string().enum(['default', 'selected', 'hidden', 'disabled']))
    .prop('title', S.string()).description('Title of environment. Contains `url`, `path`, `baseHref` or any fields of config. Mustache render, color access via `color`, e.g., "{#color.red}{name}{/color.red}"')
    .required(['endpoint', 'ssh']);

const deployOptions = S.object()
    .prop('ngProject', S.string()).description("`ng build [ngProject]`")
    .prop('nodeOptions', S.object().additionalProperties(S.object().raw({
        type: ['string', 'number', 'boolean']
    })))
    .extend(ngBuildDeployOptions);

const angularSchemaJson = S.object()
    .title('NgxSshDeployAngularOptions')
    .prop('$schema', S.string())
    .prop('build', S.anyOf([
        S.boolean(),
        S.object().additionalProperties(false)
            .prop('always', S.boolean())
            .prop('verbose', S.boolean()),
    ]))
    .extend(ngBuildDeployOptions);

const schemaJson = S.object()
    .title('NgxSshDeployConfig')
    .definition('environment', deployOptions)
    .prop('$schema', S.string())
    .prop('environments', S.object()
        .additionalProperties(S.ref('#/definitions/environment')))
    .prop('defaultEnvironment', singleOrArrayString)
    .required(['environments']);

const packageJson = require('./package.json');

const ngAddJson = S.object()
    .id(packageJson.name + '-ng-add-schematic')
    .title(packageJson.name + ' ng-add schematic')
    .additionalProperties(false)
    .prop('project', S.string())
    .prop('endpoint', S.string().description('SSH endpoint').examples(['ec2-user@ec2-127-0-0-1.example.compute.amazonaws.com/sub']))
    .prop('pemFile', S.string().description('PEM file path on local machine'))
    .prop('sshPath', S.string().description('Path to deploy on target machine'));

async function writeSchema() {
    const fsExtra = require('fs-extra');
    const fsPath = require('path');

    const write = async (path, schema) => {
        await fsExtra.ensureDir(fsPath.dirname(path));
        await fsExtra.writeJSON(path, schema.valueOf(), { spaces: 2 });
    };

    await Promise.all([
        write('dist/builders/deploy.schema.json', angularSchemaJson),
        write('dist/schematics/ng-add.schema.json', ngAddJson),
        write('ngx-ssh-deploy.schema.json', schemaJson),
    ]);
}

async function writeTypes() {
    const fsExtra = require('fs-extra');
    const schemaToTs = require('json-schema-to-typescript');

    const sources = await Promise.all(Object.entries({
        DeployEndpoint: deployEndpoint,
        DeployEnvironment: deployOptions,
        DeployProjectConfiguration: ngBuildDeployProjectConfiguration,
        AngularBuilderOptions: angularSchemaJson,
        NgAddOptions: ngAddJson,
    }).map(([infName, type], idx) => schemaToTs.compile(type.valueOf(), infName, {
        bannerComment: idx > 0 ? '' : undefined,
    })));

    await fsExtra.writeFile('src/schema.ts', sources.join(''));
}

module.exports = { deployOptions: ngBuildDeployOptions, writeSchema, writeTypes };
